

# 伪元素



这里使用 `:` 和 `::` 都是可以的。
```css
/* p的第一个字符 */
p:first-letter


.help:before


.help:after


div p:first-child


div p:last-child

/* 第几个孩子  默认从1 开始*/
div p:nth-child(1)
```

