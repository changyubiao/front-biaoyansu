


## 15.10 成品图 
实现了web端 和手机端的 响应式布局，对新闻网站首页，详情页，登录，注册页 实现，并且页面会自动适配 不同的 屏幕，手机，电脑等。



### 首页
![index](./result-img/index.png)

### 首页详情
![index](./result-img/index-detail.png)


### 新闻详情
![index](./result-img/news-detail.png)




### 首页手机端-1
![index](./result-img/index-mobile-1.png)


### 首页手机端-2
![index](./result-img/index-mobile-2.png)



### 登录页
![index](./result-img/login.png)


### 登录页 手机端
![index](./result-img/login-mobile.png)




### 注册页
![index](./result-img/signup.png)


### 注册页 手机端
![index](./result-img/signup-mobile.png)






## 参考链接
- [bootstrap响应式工具](https://v3.bootcss.com/css/#responsive-utilities)
