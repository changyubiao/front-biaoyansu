# const 关键字介绍

## 定义常量
const 关键字用来定义常量，定义值不可以修改了。

```javascript
// 定义一个常量
const love = 'love you';

console.log(love);

// Uncaught TypeError: Assignment to constant variable.
love  = 'love me';
```

第二次修改`love`这个变量的时候就直接报错了，说常量不能被赋值的。

## 初始化 并且赋值
```javascript

// Uncaught SyntaxError: Missing initializer in const declaration 
const love ;

// love = 'love you';
``` 
如果定义常量不直接初始化，也要报错。 const 要求定义的变量 和初始化要一行完成。而不是 先定义后初始化。  这个和使用 var 关键字是有区别的。

```javascript

var love ;

love = 'love you';
console.log(love);  // love you 
```

使用`var` 声明 和 赋值 可以分别 进行。 但是 `let` 并不能这样做。


## const 指向可变类型

const 声明的变量可以指向任何类型除了 数字，字符串，数组 ，对象等。

```javascript
const num = 100;

const love = 'love';

const fruits = ['apple','orange','banana'];

const fruits = ['apple','orange','banana'];


var user = {
	name:'frank',
	age:18,
	hobby:'gym'
}

console.log('user:',user);
const person = user;
```

现在问题来了 如果常量 被赋值为一个可变的对象，比如说object 这种情况。我们可以直接修改 object 来改变值吗？ 

```js
var user = {
	name:'frank',
	age:18,
	hobby:'gym'
}

console.log('user:',user);  //{name: "frank", age: 18, hobby: "gym"}
const person = user;

// 修改 user.age 
user.age = 20;
console.log('user:',user); //{name: "frank", age: 20, hobby: "gym"}
```

我们发现是可以修改的，所以 这里说定义常量是指 指向的引用不能改变，而不是引用的值不能改变。 对于这个例子就是 person -> user 这个关系没有变化，而user 本身被修改 person 是管不了的。const 只能保证 person -> user 这个关系不变。

这里我改变了person 的指向，重新赋值。
```javascript
person = 'Liu laoda';
```
![img](image/img-01.png)



## const 的作用域
const 定义的变量 与 var 的作用域一样，也是在自己的块作用域里面

```javascript

var flag = true;

if (flag) {
	let title = 'frank';
	console.log("if inner:title:", title);  // if inner:title: frank
}
// Uncaught ReferenceError: title is not defined
console.log("if outter:title:", title);  
```

`title` 出了 `if` 语句块就不可见了.






