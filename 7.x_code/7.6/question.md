

## js 使用 name 作为变量名的问题？

使用 `name` 作为全局变量的问题？

```js

function Yo() {
	var name = new Array("Tom", "Mark", "Curry");
	for (var i = 0; i < name.length; i++) {
		console.log(name[i]);
	}
}

Yo();
```