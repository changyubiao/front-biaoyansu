

// var len = 'Frank'.length;
// console.log('len:',len);



// exp1 
// let {length} = 'Frank'
// console.log('length:',length);




// 解构数组、在形参外加[ ]、解构对象 加{ }。


// exp2.
// var [a,b,c] = 'Iam';
// console.log(a,b,c);



//  数组中参数
// var arr = [1,2];
// function Hello(a,b){
// 	console.log('a:',a);
// 	console.log('b:',b);
// }

// Hello(arr[0],arr[1]);



// 改成下面的传参形式，自动解构赋值了。
// var arr = [1,2];
// function Hello([a,b]){
// 	console.log('a:',a);
// 	console.log('b:',b);
// }
// Hello(arr);




// exp3. 数组传参要关注顺序 ,我们可以传入 对象
// 这样传参的好处，就是不需要关注传参的顺序，只要有这个名字即可
// var obj ={
// 	a:10,
// 	b:20,
// }

// function Hello({a,b}){
// 	console.log('a:',a);
// 	console.log('b:',b);
// }
// Hello(obj);


// exp3-1. 甚至可以设置默认值
// 此时并没有b 这个变量
var obj ={
	a:10
}

function Hello({a,b=100}){
	console.log('a:',a);  // 10
	console.log('b:',b); //100 use 默认值
}
Hello(obj);
