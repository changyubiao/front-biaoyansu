var title = '标题：上海新增本土确诊1006例 无症状感染者23937例';

var tpl1 = '<div>' +
	'<span>' + title + '</span>' +
	'</div>';


// ${title}
var tpl2 = `
<div>
	<span>${title}
	</span>
</div>
`

console.log("tpl1:", tpl1);
console.log("tpl2:", tpl2);

// console.log(tpl2)



// 嵌套模板
let tpl3 = `
<div>
	<span>${title + `
		<span> 2022 </span>`}
	</span>
</div>
`;


console.log("tpl3:", tpl3);

let confirmed = 1006;
let asymptomatic = 23937;
var sentence = `
2022年4月9日0—24时, 标题：上海新增本土确诊${confirmed}例 无症状感染者${asymptomatic}例
`
console.log('sentence:', sentence);
