# let 命令的作用 




## 限制变量的作用域
let 主要也是用来定义变量的，看下面的代码 使用var 来定义变量i,来遍历数组。
```javascript
arr = ['Liu Laoda', 'Lile', 'Frank']
for (var i = 0; i < arr.length; i++) {
	console.log(arr[i], i);
}

console.log("i:", i);   // i: 3
````
我们发现 当for 结束的时候， `i`变量 还是有效的。

如果我们希望 i 在出了 `for` 循环后失效， 该如何做呢， 此时可以使用 `let` 定义变量 `i`


```javascript

arr = ['Liu Laoda', 'Lile', 'Frank'];
for (let i = 0; i < arr.length; i++) {
	console.log(arr[i], i);
}

// 此时已经 没有 i， 直接报错
console.log("i:", i);  //Uncaught ReferenceError: i is not defined
```


来看第二个例子,

使用 var 来定义一个 块里面的变量，出了 这个块，之后还是可以进行访问的。这里使用if 语句块。
```javascript 

var flag = true;

if (flag) {
	var title = 'frank';
	console.log("if inner:title:", title);  // if inner:title: frank


}
console.log("if outter:title:", title);  // if outter:title: frank

```

如果 把 title 换成 `let` 就不一样了,此时 title 在出了 if 语句后 title 这个变量 就不复存在了。 

```javascript 

var flag = true;

if (flag) {
	let title = 'frank';
	console.log("if inner:title:", title);

}

console.log("if outter:title:", title); //main.js:41 Uncaught ReferenceError: title is not defined

```

总结 let 可以把变量的作用域限制在一个块中，可以更加有效控制变量的作用范围。比如在for 循环中 使用let 可以有效的控制循环变量。



## 有效防止变量重复定义

看下面的代码 
```javascript


var flag = true; //true

function foo(){
	console.log('Yo.')
}


foo();
// ... many lines 
console.log(flag);



var flag = 'frank';  
console.log(flag);  //frank
```

我重复定义了flag 变量， 然后也没有任何问题，不会报错，并且直接覆盖了flag 变量，这样比较危险，比如你有多个js 文件，结果每个js 都有变量叫 flag 就会出现名称
 相同 而覆盖的问题，并且也不会报错。 为了解决上面的问题，我们可以使用let 关键字 来进行定义变量，这样 第二次定义的时候，就会报错。 `Identifier 'flag' has already been declared `
 
```javascript
let flag = true;

function foo(){
	console.log('Yo.')
}


foo();
// ... many lines 
console.log(flag);


// Uncaught SyntaxError: Identifier 'flag' has already been declared 
let flag = 'frank';  
console.log(flag);

```



## var 与let 在function的区别 

```javascript


function foo(){
	var hobby='sing';
	console.log(hobby);
}

foo()

// Uncaught ReferenceError: hobby is not defined
console.log(hobby); 

```
在函数中定义 hobby 使用var，在函数外不能使用的,not defined.

使用let 也是一样的效果，函数里面的定义，出了函数，就不存在了，即函数里面的定义域 作用范围 仅在函数内部使用。不能出函数的，无论使用 var 还是 let 定义都一样。
```javascript

function foo(){
	let hobby='sing';
	console.log(hobby);
}

foo()

// Uncaught ReferenceError: hobby is not defined
console.log(hobby); 
```







## var 定义变量问题 


看下 下面代码片段

```javascript


var hobby = 'gym';

function foo() {
	
	console.log('hobby 111:', hobby);  //hobby 111: gym

	console.log('hobby 222:', hobby);  //hobby 222: gym
}

foo();

```
这段代码 在函数里面打印 函数外面定义的变量，我们很容易理解 这里 就正常打印出来了。

 
 这代码里面我们添加了 if 语句块，在if语句我们定义了一个变量 也叫hobby ,此时会打印什么呢 ？
 
 
 ```javascript
 
var hobby = 'gym';

function foo() {
	
	console.log('hobby 111:', hobby);  //hobby 111: undefined

	if (false) {
		var hobby = 'sing';
		console.log('hobby:000:',hobby);

	}
	console.log('hobby 222:', hobby);  //hobby 111: undefined

}
// call function
foo();
 ```
 此时打印的是，`undefined`, 这个可能你会觉得奇怪，为啥不是gym  ？ 
  
这个需要有一定预编译的知识，才能理解，在函数执行的时候 会有一个AO，在函数内部 会有一个预编译的过程 ，函数里面有 hobby 的定义，这个会放到 function AO 上，然后 函数执行 的时候 ，编译器会发现 有 hobby 的定义，所以会找 AO 上的hobby ，此时发现 没有赋值，因为 if语句块 永远不会执行。 所以 打印结果就是 `undefined`


此时如果里面使用`let` 来定义变量 ，结果就是符合我们预期的啦，

在`if` 语句中，定义的变量，出来之后 这个变量就可以认为 消失了，或者不在作用域范围内了。

```javascript

var hobby = 'gym';

function foo() {
	console.log('hobby 111:', hobby);  //hobby 111: gym


	if (false) {
		let hobby = 'sing';
		console.log('hobby:000:',hobby);
	}
	console.log('hobby 222:', hobby);//hobby 222: gym
}

foo();
```






## 再看一个例子 var 与let 定义的区别 

```javascript
console.log('hobby:',hobby);  //hobby: undefined
var hobby= 'swim';
```
这里 可以打印 hobby ，只是没有值而已，这也是和预编译相关。 




如果使用let 就可以直接报错，我们可以提前发现问题，hobby 没有定义呢，你就要访问 这是不允许的。
```javascript
// Uncaught ReferenceError: Cannot access 'hobby' before initialization
console.log('hobby:',hobby);  
let hobby= 'swim';
```

