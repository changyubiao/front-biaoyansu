var user = {
	name: 'frank',
	hobby: 'gym',
	age: 18
}


// exp1. 这种是根据对象的key 来结构的，是根据名称来解构的。
// let {name,age} = user;
// console.log('name:',name);
// console.log('age:',age);



// exp2. 结构对象的时候 只看 对象的key 如果名称相同 就赋值即可，没有就是undefined 
// let {name} = user;
// console.log('name:',name);
// console.log('age:',age);

// let {a,b,age} = user;
// console.log('a:',a);//undefined
// console.log('b:',b);//undefined
// console.log('age:',age); // 18


// exp3 结构的时候 我有时候可能想换个名字 我该 如何做呢 ? 
// var user = {
// 	name:'frank',
// 	hobby:'gym',
// 	age:18
// }



// let {hobby:Hobby} = user;
// // 此时并没有定义 hobby 这个变量，只有 大写 Hobby这个变量
// // Uncaught ReferenceError: hobby is not defined
// console.log('hobby:',hobby);//error 
// console.log('Hobby:',Hobby); //Hobby: gym


//  == 此时先定义，然后结构 也会报错，重新定义了。
// let hobby ;
// Uncaught SyntaxError: Identifier 'hobby' has already been declared (at main.js:44:6)
// let {hobby} = user;


// == 这样做是可以的，此时相当于定义了一个 hobby ,Hobby 这两个变量
// let hobby ;
// let {hobby:Hobby} = user;
// console.log('Hobby:',Hobby); //Hobby: gym



// exp4.  解构 修改已经 定义的变量的情况

// // var user = {
// // 	name:'frank',
// // 	hobby:'gym',
// // 	age:18
// // }

// let hobby = 'gan fan';
// console.log('hobby:',hobby);
// // 修改hobby 的值，可以加上 小括号
// ({hobby} = user);
// console.log('hobby:',hobby);




// exp5. 来看一个比较复杂的情况

// var obj = {
// 	arr: [
// 		'Liu laoda',
// 		'Li le',
// 		'Wei,liang',
// 		{
// 			tel: 123455
// 		},
// 		'Frank',

// 	],
// }

// let {
// 	arr: [, , , tel_info, frank]
// } = obj;
// console.log(tel_info);
// console.log(frank);



// exp5-1.
// var arr = [
// 	1,
// 	2,
// 	{
// 		tel: 123,
// 		age: 10,
// 		hobby: 'swim'
// 	}
// ];

// let [, , user_info] = arr;
// console.log(user_info);


// exp6. 默认值 
// 如果找不到对应的key ,给一个默认值
// let {a=10,b=10} = {a:100};
// console.log('a:',a);  // 100
// console.log('b:',b); // 10

// 也可以重命名 设置默认值 
// let {a:A=10,b=10} = {a:100};
// console.log('A:',A);  // 100
// console.log('b:',b); // 10

// exp7 实际项目中的使用
// var res = {
// 	code: 200,
// 	id: 10,
// 	data: [
// 		{
// 		user: 'frank'
// 	},		{
// 		user: 'Liu laoda'
// 	},		{
// 		user: 'Yang laoliu'
// 	},
// 	]
// }

// old style 
// var  code = res.code ;
// var  id  = res.id ;
// var  data = res.data ;
// console.log(code,id,data);

// new style 
// let {code,id,data} = res ;
// console.log(code,id,data);


// exp7-2 从对象里面结构一些函数 自己使用
let {floor,pow,abs} = Math ;
console.log('abs(-1.9):',abs(-1.9));
console.log('pow(2,3):',pow(2,3));
