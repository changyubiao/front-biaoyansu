// var a = 1,
// 	b = 2,
// 	c = 3;

// console.log(a,b,c);



// ES 6 里面 可以这样赋值 ，同时赋值多个变量
// var [a,b,c] = [1,2,3];
// console.log(a,b,c);



// exp2.嵌套的结构也是可以的
// var [[a,b],c] = [[1,2],3];
// console.log(a,b,c);



// exp3. 只想要 某几个变量, 这样会只有 1，3 分别对应 a,c 
// var [a,,c] = [1,2,3,4,5,6];
// console.log(a,c);


// exp4. 如果 想 把第一个元素拿出，后面作为一个整体
// var [first,...arr] = [1,2,3,4,5,6];
// console.log(first);
// console.log(arr);  // [2, 3, 4, 5, 6]
// console.log(arr.constructor.name =='Array'); // true 



// exp5. 设置default ,如果长度不相等的情况下可以 指定 默认值
//  结构赋值 
// var arr = [1, 2, 3];
// var a = arr[0];
// var b = arr[1];
// var c = arr[2];

// if (arr[3]) {
// 	var d = arr[3];
// } else {
// 	var d = 'default';
// }
// console.log(a, b, c); // 1 2 3 
// console.log(d); // default 



// ES6 新的方式赋值
// var arr = [1, 2, 3];
// var [a, b, c = 'default', d = 'default'] = arr;
// console.log(a, b, c); // 1 2 3 
// console.log(d); // default 





// exp6. 长度不对等的情况下，这种 c就等于 undefined
// let [a,b,c] = [1,2];
// console.log(a,b,c); //1 2 undefined


// let [a,b,c] = [1,2,null];
// console.log(a,b,c); //1 2 null


var arr = [1, 2];
if (typeof arr[2] === 'undefined') {
	arr[2] = undefined;
}
console.log(arr);
