# 数组的解构赋值

<!-- Destructuring_assignment
 -->


 曾经我们定义变量,一起定义 需要下面这样写。

 ```javascript 
var a = 1,
	b = 2,
	c = 3;
	
console.log(a,b,c);
 ```

 在ES6 标准里面 有一种方式 可以直接赋值

##  exp1. 数组的解构赋值

 ```js
// ES 6 里面 可以这样赋值 ，同时赋值多个变量
var [a,b,c] = [1,2,3];
console.log(a,b,c); // 1 2 3
 ```

##  exp2. 嵌套的结构也是可以的

 ```js
 // exp2.嵌套的结构也是可以的
 var [[a,b],c] = [[1,2],3];
 console.log(a,b,c); // 1 2 3

 ```

 

##  exp3. 只想要某几个变量, 这样会只有 1，3 分别对应 a,c

 ```js
// exp3. 只想要 某几个变量, 这样会只有 1，3 分别对应 a,c
var [a,,c] = [1,2,3,4,5,6];
console.log(a,c);  // 1 3

 ```

## exp4. 如果想把第一个元素拿出，后面作为一个整体.

我们可以使用 `...变量名` 这样其他的值就会变成一个数组赋值给 `变量名` ,这里 的变量名为 `arr`

 ```js
// exp4. 如果 想 把第一个元素拿出，后面作为一个整体
var [first,...arr] = [1,2,3,4,5,6];
console.log(first);
console.log(arr);  // [2, 3, 4, 5, 6]
console.log(arr.constructor.name =='Array'); // true 
 ```

 

## exp5. 设置default ,如果长度不相等的情况下可以指定默认值

 在ES5标准中 我们可能需要赋值的是否需要判断 是否有值，然后在进行赋值。

 ```js
// exp5. 设置default ,如果长度不相等的情况下可以 指定 默认值
//  结构赋值 
var arr = [1, 2, 3];
var a = arr[0];
var b = arr[1];
var c = arr[2];

if (arr[3]) {
	var d = arr[3];
} else {
	var d = 'default';
}
console.log(a, b, c); // 1 2 3 
console.log(d); // default 

 ```

 在ES6 标准中 可以设置默认值

 ```js
 // ES6 新的方式赋值
var arr = [1, 2, 3];
var [a, b, c = 'default', d = 'default'] = arr;
console.log(a, b, c); // 1 2 3 
console.log(d); // default 
 ```

 如果 `arr` 里面可以有对应的值，使用 `arr` 元素的值，如果没有，则使用默认值。上面的例子，c,d 有默认值，
 但是c 可以从 `arr` 获取，所以是使用`arr[2]`. d 没有办法从`arr` 赋值，就使用默认值。

 

## exp6. 长度不对等的情况下

长度不对等的情况下，最后没有赋值的变量 `c` 就等于 `undefined`

在 ES5 标准 中，如何赋值，需要判断 undefined

```js
var arr = [1, 2];
if (typeof arr[2] === 'undefined') {
	arr[2] = undefined;
}
console.log(arr);
```

  在 ES6中，如果长度不对等的情况，变量直接为 undefined

```js
 // exp6. 长度不对等的情况下，这种 c就等于 undefined
 let [a,b,c] = [1,2];
 console.log(a,b,c); //1 2 undefined
```

 

 如果`arr` 的长度和变量个数对应起来，则就可以完全赋值，并且 `null` 也是一种值，不会变成 `undefined`。

 ```js
let [a,b,c] = [1,2,null];
console.log(a,b,c); //1 2 null
 ```
 
 
 
 ## 参考文档
[mdn 解构赋值](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)