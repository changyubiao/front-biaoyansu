var arr = [1, 2, 3, 3, 3];

// 构造方法
let person = new Set([1, 2, 3, 3, 3]);



// console.log('arr:', arr);
console.log('person:', person);


// 长度 size 属性  vs arr.length
// console.log(person.size);



//添加元素
person.add('frank');

console.log('person:', person);

console.log('person.size:', person.size);


// 删除元素 
person.delete('frank')
console.log('person:', person);

// 判断元素是否在 set 中  
console.log('person.has(6):', person.has(6));
person.add(6);
console.log('person:', person);
console.log('person.has(6):', person.has(6));



// 清空元素
person.clear()
console.log('person:', person);
