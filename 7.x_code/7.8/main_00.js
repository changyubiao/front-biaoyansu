// https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Symbol

// let a = Symbol();
// let b = Symbol();

// console.log(a===b);
// console.log(a==b);




/// file1.js

let name = Symbol();

{
	var person = {};

	person[name] = 'file1.js';

}


// file2.js
{
	person['name'] = 'file2.js';

}


// {name: "file2.js", Symbol(): "file1.js"}
console.log('person:', person);
