// https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Symbol

// let a = Symbol('a');
// let b = Symbol('b');

// console.log(a===b);  // false
// console.log(a==b);  // false


// 我们通过Symbol创建的值一定是一个唯一的值。


// const obj = {};
// obj[Symbol()] = '橙子';
// obj[Symbol()] = '柚子'
// console.log(obj)


let obj ={[Symbol.toStringTag]:'Xobject 111111111'}
console.log(obj.toString())




// // file1.js
// let name = Symbol();

// {
// 	var person = {};
// 	person[name] = 'file1.js';
// 	console.log('person[name]:', person[name]);  //person[name]: file1.js


// }


// // file2.js
// {
// 	let name = Symbol();
// 	person[name] = 'file2.js';
// 	console.log('person[name]:', person[name]); // person[name]: file2.js

// }

// // {Symbol(): "file1.js", Symbol(): "file2.js"}
// // console.log('person:', person);
// console.log('person[name]:', person[name]);









/*

我们使用Symbol值作为对象的属性名，这个属性通过传统的for...in...是无法拿到的，通过Object.keys也是获取不到的Symbol类型的属性名，使用JSON.stringify序列化对象，Symbol类型的属性名也会被忽略掉。因此Symbol类型的属性特别适合作为对象的私有属性！

作者：Jser阿武
链接：https://juejin.cn/post/6979359271901200398
来源：稀土掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。


*/ 