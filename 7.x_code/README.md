
# ES6 标准 学习笔记

这里是记录 和 表严肃学习 ES6 新特性的笔记，网站对应的地址[ES6精讲](https://biaoyansu.com/7.x)



ES6 对js 做了很多的加强，比如 let,const 来控制变量的作用域，已经变量的解构赋值，新增的字符串方法. `string.includes`,`string.startsWith`,`string.endsWith`,`string.repeat`. 新增模板字符串 更加方便的格式化字符串了。 新增了 Symbol 这种数据类型，这种类型可以方便标识一个唯一的值。 新增Proxy 来控制变量的取值，赋值等。 还有新增了 Set 这种集合的容器。



## 第一课 7.1 let 关键字讲解

let 关键字介绍，并且对比了与 var的区别。 [let_note](front-baiyansu/7.x_code/7.1/let_doc.md)

## 第二课 7.2 const关键字


## 第三课 7.3  变量的解构赋值（数组）
ES6 支持 变量的解构赋值

## 第四课 7.4  变量的解构赋值（对象）
## 第五课 7.5 变量的解构赋值（其他）
## 第六课  7.6 新增字符串方法
## 第七课 7.7 模板字符串
## 第八课 7.8 Symbol类型
## 第九课 7.9 Proxy
 [mdn Proxy](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Proxy)
 Proxy 对象用于创建一个对象的代理，从而实现基本操作的拦截和自定义（如属性查找、赋值、枚举、函数调用等）。
## 第十课 7.10 Set 




## 课程链接
[ES6精讲](https://biaoyansu.com/7.x)