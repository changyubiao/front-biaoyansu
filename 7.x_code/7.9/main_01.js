/*
https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Proxy

*/ 

const handler = {
	get: function(obj, prop) {
		return prop in obj ? obj[prop] : 'default_value';
	}
};

const p = new Proxy({}, handler);
p.a = 1;
p.b = undefined;

// console.log(p.a, p.b); // 1, undefined

console.log('c' in p, p.c); // false  default_value
