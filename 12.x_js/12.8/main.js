// for  loop 




// exp1.
// var i;
// var result = [
// 	'索尼精选-正版超高品质Hi-Res DSD环绕声音乐',
// 	'一听音乐网_歌曲大全::免费网络在线正版音乐站',
// 	'适合朗诵的背景音乐-2022经典适合朗诵的背景音乐一键下载',
// 	'千千音乐',
// 	'网易云音乐',
// 	'音螺·数字音乐新场景',
// ]

// for (i = 0; i < result.length; i++) {
// 	// console.log(i);
// 	console.log(result[i]);
// }



// exp2.
// const arr = ['y', 'o', 'o', 'o'];

// // 记录最终结果
// let result = '';

// // 循环，i++ 相当于 i = i + 1
// for (let i = 0; i < arr.length; i++) {
//   // 取到每一项
//   const it = arr[i];
//   // 加到最终结果里，result += it 相当于 result = result + it
//   result += it;
// }

// console.log(result);  //yooo 





// exp3. 
// const arr = [1, 3, 2, 7, 6, 8];

// // 用一个数组盛放最终结果
// let evens = [];

// // 循环，i++ 相当于 i = i + 1
// for (let i = 0; i < arr.length; i++) {
// 	// 取到每一项
// 	const it = arr[i];

// 	// 如果当前项与2求余等于0，就说明是偶数，
// 	if (it % 2 === 0) {
// 		// 那就应该添加到 evens 数组里
// 		evens.push(it);
// 	}
// }

// console.log(evens);  // [2,6,8]




// exp4. 
// 用数组存放最终结果
// const result = [];
// // 设置最大数，作为循环条件
// const max = 5;

// // 从1开始，循环到5就结束
// for (let i = 1; i <= max; i++) {
// 	// 把每个数推到结果数组中
// 	result.push(i);
// }
// console.log(result); // 1 2 3 4 5 




// // exp5. 
// const arr = ['a1', 'a2', ['b1', 'b2', ['c1', 'c2', 'c3'], 'b3'], 'a3'];
// // 用于记录爷爷，爸爸，孩子的数据
// const grands = [] // 爷爷数组，这里面只装爷爷
// const dads = [] // 爸爸数组，这里面只装爸爸
// const children = [] // 孩子数组，这里面只装孩子

// // 从最外层开始遍历
// for (let i = 0; i < arr.length; i++) {
// 	// 取到爷爷辈的元素
// 	const grand = arr[i]
// 	// 如果是字符串
// 	if (typeof grand === 'string') {
// 		// 就推到爷爷数组
// 		grands.push(grand)
// 	} else { // 否则肯定是嵌套数组
// 		// 那就继续循环
// 		for (let j = 0; j < grand.length; j++) {
// 			// 取到爸爸辈的元素
// 			const dad = grand[j]
// 			// 如果是字符串
// 			if (typeof dad === 'string') {
// 				// 就推到爸爸数组
// 				dads.push(dad)
// 			} else { // 否则肯定是嵌套数组
// 				// 那就继续循环
// 				for (let k = 0; k < dad.length; k++) {
// 					// 取到孩子辈的元素
// 					const child = dad[i]
// 					// 推到孩子数组
// 					children.push(child)
// 				}
// 			}
// 		}
// 	}
// }

// // join() 可以将数组里的每一项连接成字符串，可以指定用什么连接
// // 比如说
// // ['a', 'b'].join('-') 会生成 'a-b'
// // ['a', 'b'].join('@') 会生成 'a@b'
// // 此处我们用 ', ' 来连接每一个以辈分分类的数组
// const result = `
// 第1级：${grands.join(', ')}
// 第2级：${dads.join(', ')}
// 第3级：${children.join(', ')}
// `
// // 最后打印结果
// console.log(result)




/*

6 --10 
*/


// exp6. 
// const users = [{
// 		name: '王花花',
// 		salary: 100,
// 	},
// 	{
// 		name: '李拴蛋',
// 		salary: 200,
// 	},
// 	{
// 		name: '牛备备',
// 		salary: 300,
// 	},
// ]

// // 准备结果数组
// const result = [];

// // 遍历所有用户
// for (let i = 0; i < users.length; i++) {
// 	// 取到当前用户
// 	let it = users[i];

// 	// 如果其薪水大于100
// 	if (it.salary > 100) {
// 		// 就推到结果数组中
// 		result.push(it);
// 	}
// }
// console.log(result);




// exp7 
// const arr = [{
// 		name: '王花花',
// 	},
// 	{
// 		name: '王拴蛋',
// 	},
// 	{
// 		name: '牛备备',
// 	},
// ]

// // 准备结果
// const result = []

// // 遍历所有用户
// for (let i = 0; i < arr.length; i++) {
// 	const it = arr[i];
// 	// 如果当前用户姓名以"王"开头
// 	if (it.name.startsWith('王')) {
// 		// 就推到结果数组中
// 		result.push(it);
// 	}
// }

// console.log(result);



// exp8. 

// const arr = [
// 	{
// 		name: '王花花',
// 		playing: ['CS', '红警'],
// 	},
// 	{
// 		name: '李拴蛋',
// 		playing: ['守望屁股', '吃鸡', 'CS'],
// 	},
// 	{
// 		name: '牛备备',
// 		playing: ['红警', '吃鸡'],
// 	},
// ]

// /**
//  * 单个关键词搜索
//  * @param {string} keyword
//  */
// function search(keyword) {
// 	const result = [];
// 	// 循环每个用户
// 	for (let i = 0; i < arr.length; i++) {
// 		let user = arr[i];
// 		// 看每个用户的 playing 中是否包含传进来的关键词
// 		if (user.playing.includes(keyword)) {
// 			// 如果包含，就把当前用户的名字推到结果中
// 			result.push(user.name);
// 		}
// 	}

// 	return result;
// }

// console.log(search('CS'));
// console.log(search('吃鸡'));
// console.log(search('红警'));
// console.log(search('守望屁股'));





// exp9 .

const arr = [{
		name: '王花花',
		playing: ['CS', '红警'],
	},
	{
		name: '李拴蛋',
		playing: ['守望屁股', '吃鸡', 'CS'],
	},
	{
		name: '牛备备',
		playing: ['红警', '吃鸡'],
	},
	{
		name: '木哈哈',
		playing: ['红警', 'CS', '吃鸡'],
	},
]

/**
 * 严格搜索
 * 满足所有keyword的搜索
 * @param keywords
 */
function search(keywords) {
	const result = []

	// 遍历所有用户
	for (let j = 0; j < arr.length; j++) {
		const user = arr[j]
		// 看这个用户玩的所有游戏是否（完全）包含所有 keyword
		if (isSuperSet(user.playing, keywords)) {
			// 如果包含就将用户名推进结果
			result.push(user.name)
		}
	}

	// 返回结果
	return result
}




/**
 * a 是否是 b 的超集
 * 判断数组 a 是否完全包含数组 b 中的每一个元素（多出来没事儿）
 * @param playing {Array}
 * @param keywords {Array}
 * @return {boolean}
 */

function isSuperSet(playing, keywords) {

	for (var i = 0; i < keywords.length; i++) {
		keyword = keywords[i];
		if (!playing.includes(keyword)) {
			return false;
		}
	}
	return true;
}


console.log(search(['CS', '红警']))
console.log(search(['吃鸡']))
console.log(search(['守望屁股', '吃鸡', 'CS']))
