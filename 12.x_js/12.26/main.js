

//  https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Function/bind


// var name = undefined;
function Yo(name,a,b) {
	// console.log(this);
	console.log(a,b);
	console.log('Yo,  '+ name +'!' +'  I am ' + this.name);
}



var whh = {
	name: 'whh'
};

var lsd = {
	name: 'lsd'
};

var frank = {
	name:'frank'
}

// Yo.call(lsd);

// Yo.call(whh);

// Yo.call();
// Yo();

// call 方式调用
// Yo.call(frank,'Liu laoda',1,2);
// Yo.call(lsd,'xiao ming',10,20);


// apply 参数后面是数组的形式
// Yo.apply(frank,['Liu laoda',1,2]);



// bind 绑定参数与call 一样， 只不过返回一个函数，而不是立即执行
// 绑定时候 就传递参数 ,绑定完成后，后面的参数的值 就确定了，不会发生变化了
var Yo2 = Yo.bind(frank,'Liu laoda 1',5,10);
Yo2();  //Yo,  Liu laoda 1!  I am frank

Yo2('Li le',1,2); //Yo,  Liu laoda 1!  I am frank


// 绑定时候 只指定上下文 ，而不传递参数的情况
// var Yo3 = Yo.bind(frank);
// Yo3('Liu laoda 2',10,210);

// console.log("this.name:",this.name);