// 给函数赋能 ，this 取决于谁来调用这个 函数，不同的对象调用这个函数，
// 获取的 this.name 不同

function Yo() {

	console.log('Yo, ' + this.name);
}


var whh = {
	name: 'whh'
};

var lsd = {
	name: 'lsd'
};


whh.yo = Yo;
lsd.yo = Yo;


whh.yo();
lsd.yo();
