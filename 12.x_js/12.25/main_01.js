// 'use strict'

var person = {
	lname: 'chang',
	fname: 'yubiao',
	
	
	fullname: function() {
		console.log('this:',this);
		return this.lname + this.fname;
	},
	
	
	a:{
		fullname: function() {
			console.log('this:',this);
			return this.lname + this.fname;
		}
		
	}

}

// var fullname= person.fullname;
// // this == windows  or undefined
// console.log(fullname())




console.log(person.fullname())

// 此时 this == a 等于父级 对象
// console.log(person.a.fullname());



// part 2 
// function User(name){
// 	console.log(this);
// 	this.name = name ;
// 	this.age = 18	
// }


// 这样调用 this == User, 这样是调用构造函数
// new User('frank');

// 这样调用 this== window，这样是普通的函数调用,在严格模式下 是 undefined
// User('frank')





// exp3...
// this window 直接调用
// function a(){
// 	// this  // window
// }

// (function(){
// 	//  this   // window
// })