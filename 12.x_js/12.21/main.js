function User(name, gender, age) {

	this.name = name;
	this.gender = gender;
	this.age = age;


	this.eat = function() {
		console.log(this.name + ' ' + 'mia miao.');

	}

	// this.greet = function() {
	// 	console.log('I am ' + this.name + ' I am ' + this.age + ' years old')
	// }

}


User.prototype.greet = function() {
	console.log('I am ' + this.name + ' I am ' + this.age + ' years old')
};


var Laoda = new User('Liu,xiaolu', 'male', 20);
// console.log(LiuLaoda);


// LiuLaoda.greet();


var frank = new User('Frank', 'male', 20);

// frank.greet();
// true 
// frank.__proto__ === User.prototype



console.log(Laoda.greet === frank.greet);



// frank.__proto__.constructor     ==  User 




// 原型本身就是一个对象,
// 在这个对象里面有一个 constructor 属性，即 构造器
// 函数默认有一个 prototype 属性 ，即原型
function A(){};
// A.prototype.name  ='lalala';

var a  =new A();
// var b  =new A();

// console.log(a.__proto__  === b.__proto__);

// console.log(a);


// A.prototype.constructor   // --> 指向自己的

// a.__proto__.constructor // --> 指向 A构造器

