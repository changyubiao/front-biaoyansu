# [Number - 数据类型详解](https://biaoyansu.com/12.14) [表/12.14](https://biaoyansu.com/12.14) - [JavaScrip…](https://biaoyansu.com/12.x)

安利 • 自动音频

[JS](https://biaoyansu.com/search/JS) [教程](https://biaoyansu.com/search/教程) [干货](https://biaoyansu.com/search/干货) [前端](https://biaoyansu.com/search/前端) [Web开发](https://biaoyansu.com/search/Web开发)

## 为什么会有这种类型？

因为好算。

计算机可以直接理解数字，如果你在console中输入`1 + 1`它会立即返回`2`，而如果你输入`'1' + '1'`猜猜它会返回什么？并不是我们预期的`'2'`而是`'11'` ˙Ꙫ˙

所以为了方便计算，一般在需要发生数学运算的地方我们都会使用`Number`类型，如价格、总数、折扣等等。

## 常见的`Number`类型都有哪些？

- 整数（整型）

  - 如：`1`，`-1`

  - 怎么判断一个值是否为整型？

    - `Number.isInteger(1) // true`
    - `Number.isInteger(1.1) // false`

  - 其他类型怎么转整型？

    - `parseInt('1') // 1`
    - `parseInt('1.1') // 1`
    - `parseInt('1.9') // 1`
    - `parseInt('1a') // 1`
    - `parseInt('a1') // NaN`
    - `parseInt('.9') // NaN`
    - `parseInt('0.9') // 0`

  - 真假倾向：只有

    ```
    0
    ```

    为

    ```
    false
    ```

    其他均为

    ```
    true
    ```

    - `!!0 // false`
    - `!!1 // true`
    - `!!0.1 // true`
    - `!!-1 // true`

- 小数（浮点数）

  - 如：`0.1`，`1.1`，`.1` 如果小于1，小数点前的`0`可以省略
  - 其他类型转浮点数
    - `parseFloat('1') // 1`
    - `parseFloat('1.0') // 1`
    - `parseFloat('1.1') // 1.1`
    - `parseFloat('1.1a') // 1.1`
    - `parseFloat('a1.1') // NaN`
    - `parseFloat('.1') // 0.1`

- ```
  NaN
  ```

  - 一枚奇葩的'`Number`'类型

  - `NaN` 不是一个数（**N**ot **a** **N**umber）

  - 对，它不是一个数，然后它还是种数字类型。(O_o)???

  - 一般在出错或不可预料的结果中出现，如：`'a' * 'b'`，`0 / 0`

  - `NaN === NaN // false` 永不相等

  - 判断是否为

    ```
    NaN
    ```

    可以使用

    ```
    isNaN()
    ```

    函数

    - `isNaN(1) // false`
    - `isNaN(0 / 0) // true`

## Tips

- 在其他类型前加`+`可以快速将其转换为数字类型


  ```javascript
  +'1' // 1
  
  +'' // 0
  
  +'1.1' // 1.1
  
  +true // 1
  
  +false // 0
  ```