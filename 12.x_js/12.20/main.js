// var user ={
// 	name:'frank',
// 	gender:'male',
// 	age:18
// };

// console.log(user);


// 工厂函数
function createUser(name, gender, age) {

	person = {};
	person.name = name;
	person.gender = gender;
	person.age = age;

	return person;
}

var frank = createUser('frank', 'male', 18);
console.log(frank);


// 04：54	


function User(name, gender, age) {

	this.name = name;
	this.gender = gender;
	this.age = age;

}


var LiuLaoda = new User('Liu,xiaolu', 'male', 20);
console.log(LiuLaoda);




function Dog() {

	this.name = 'huahua';
	this.age = 3;
	this.bark = function() {
		console.log(this.name + ' ' + "is barking.");
	}
}


let dog = new Dog();

console.log('dog:', dog);
