# [Object - 数据类型详解](https://biaoyansu.com/12.16) [表/12.16](https://biaoyansu.com/12.16) - [JavaScrip…](https://biaoyansu.com/12.x)

[JS](https://biaoyansu.com/search/JS) [教程](https://biaoyansu.com/search/教程) [干货](https://biaoyansu.com/search/干货) [前端](https://biaoyansu.com/search/前端) [Web开发](https://biaoyansu.com/search/Web开发)

## 为什么会有这种类型？

因为好组织。

举个栗子：

```js
// 第一种写法
var 人名 = '王花花';
var 狗的年龄 = 3;
var 人的年龄 = 20;
var 狗的职业 = '保卫员';
var 人的部门 = '序员鼓励部';
var 狗名 = '李拴蛋';
var 人的职业 = '序员鼓励师';
var 狗的部门 = '序员保卫处';

// 第二种写法
var 花花 = {
  姓名: '王花花',
  年龄: 20,
  职业: '序员鼓励师',
  部门: '序员鼓励部',
};
var 拴蛋 = {
  姓名: '李拴蛋',
  年龄: 3,
  职业: '保卫员',
  部门: '序员保卫处',
};
```

哪一种更清楚？

一目了然。

而且对象中还可以嵌套对象，这就更强大了

```js
var 花花 = {
  姓名: '王花花',
  年龄: 20,
  职业: '序员鼓励师',
  部门: '序员鼓励部',
  服务于: {
    姓名: '程续缘',
    技术: 'PHP'
  }
};
```

## 语法

### 定义对象

首先是花括号

```js
var 对象 = {

}
```

左边是键(`key`，或者叫属性名)右边是值(`value`)，每一组用英文逗号隔开

```js
var 对象 = {
  键1: '值1',
  键2: '值2'
}
```

键名一般使用英文和数字，其实使用任何字符都可以，只不过当键名中有空格和特殊字符是外部需要加引号包住

```js
{
  'yo yo yo': 'Mu Ha Ha'
}
```

### 获取对象中的属性

可以使用`.`来获取属性

```js
var obj = {
  a: 1
}

obj.a // 1
```

嵌套的对象也没问题

```js
var obj = {
  a: 1,
  b: 2,
    c: {
      c1: 666
    }
}

obj.c.c1 // 666
```

那如果有奇葩的键名怎么办？

```js
var obj = {
  'a b 😷': 'mmp'
}
```

奇葩的键名直接用`.a b 😷`取值会报错，我们可以用另一种方式来取值

```js
obj['a b 😷'] // 'mmp'
```

这种方式也同样适用与一些动态的键名

```js
var obj = {
  key1: 'val1',
  key2: 'val2',
  key3: 'val3',
}

function get_val(id) {
  var key_name = 'key' + id; // 此处将id与'key'拼接在了一起
  return obj[key_name];
}

get_val(1); // "val1"
```

### 对象中的数据类型有限制吗？

没有，以下的键值都是合法的

```js
var obj = {
  a: [1], // 数组
  b: {}, // 对象
  c: 1, // 数字
  d: true, // 布尔值
  e: 'yo', // 字符串
}
```

### 声明之后还可以加新键吗？

可以。

```js
var obj = {};
obj.a = 1; 
console.log(obj) // {a: 1}
```

### 对象中的键名可以重复吗？

不可以。后一个会覆盖前一个。

> 你一会叫王花花一会儿叫李拴蛋，你把派出所当片场啦！

