# [undefined - 数据类型详解](https://biaoyansu.com/12.19) [表/12.19](https://biaoyansu.com/12.19) - [JavaScrip…](https://biaoyansu.com/12.x)

安利 • 自动音频

[JS](https://biaoyansu.com/search/JS) [教程](https://biaoyansu.com/search/教程) [干货](https://biaoyansu.com/search/干货) [前端](https://biaoyansu.com/search/前端) [Web开发](https://biaoyansu.com/search/Web开发)

## 为什么会有这种类型？

因为JS是一门神奇的语言 :(

"undefined"这个词的字面意思是"未定义"，好的，那我声明一个变量

```js
var a; 
console.log(a); // undefined
```

`a`为什么是`undefined`？？？

"因为你那叫声明不叫定义，你还没赋值呢，如果你没赋值，那它肯定是未定义呀"

OK，我来赋值

```js
var a = undefined; // 这里竟然成功赋值了！
console.log(a); // undefined
```

也就是说我可以把它**定义**为**未定义** （黑人）？？？

我不禁哑然失笑。

> 你想重新拥有处子之身么？来我跟我嘿嘿嘿啊。
>
> -- JavaScript

## 注意

### `undefined == null` 返回 `true`

### `undefined`是个"假值"

```js
if(undefined) {
  // 这里永远都不会执行
}
```