// // 动物 》哺乳动物 》人类 》lsd

function Animal() {
}


Animal.prototype.sleep = function() {
	console.log('I am sleeping.');
}


Animal.prototype.eat = function() {
	console.log('I am eatting.')
}

// var a = new Animal();
// var b = new Animal();
// console.log(a.eat === b.eat);


function Mammal() {

	// this.suckle = function() {
	// 	console.log('I am suckling.')
	// }

}




Mammal.prototype = Object.create(Animal.prototype);
// 构造器重新指定一下
Mammal.prototype.constructor = Mammal;


Mammal.prototype.suckle = function() {
	console.log('I am suckling.')
}


// var m = new Mammal();
// console.log(m);

// 此时 为 Animal
// console.log(m.constructor);


function Person() {

}


// 继承 Mammal 
Person.prototype = Object.create(Mammal.prototype);
// 重新定义 constructor 
Person.prototype.constructor = Person;

Person.prototype.lie = function() {
	console.log("I am lying")
}



var p = new Person();
console.log('p:', p);

// 以 {a:1,name:'frank'}为原型创建对象 
// var b = Object.create({a:1,name:'frank'});
// console.log(b);




// //  a.__proto__  的 constructor  是 Array 
// var a= [];
// // var b = new Array();
// console.log(a);
