// // 动物 》哺乳动物 》人类 》lsd

// 添加属性 
function Animal(color,weight) {
	this.color = color;
	this.weight = weight;
}


Animal.prototype.sleep = function() {
	console.log('I am sleeping.');
}


Animal.prototype.eat = function() {
	console.log('I am eatting.')
}

// var a = new Animal('blue',110);
// console.log('a:',a);
// var b = new Animal();
// console.log(a.eat === b.eat);


function Mammal(color,weight) {
	// 哺乳动物
	
	Animal.call(this,color,weight);
}




Mammal.prototype = Object.create(Animal.prototype);
// 构造器重新指定一下
Mammal.prototype.constructor = Mammal;


Mammal.prototype.suckle = function() {
	console.log('I am suckling.')
}


var m = new Mammal('red',10);
console.log("m:",m);

// 此时 为 Animal
// console.log(m.constructor);





function Person(color,weight,name) {
	// Person 添加自己的属性
	Mammal.call(this,color,weight);
	this.name = name;
}


// 继承 Mammal 
Person.prototype = Object.create(Mammal.prototype);
// 重新定义 constructor 
Person.prototype.constructor = Person;

Person.prototype.lie = function() {
	console.log("I am lying")
}




var p = new Person('yellow',50,'frank');
console.log("p:",p);


var liu = new Person('yellow',60,'Liu laoda');
console.log("liu:",liu);