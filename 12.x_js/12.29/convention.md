# [命名惯例及生词表](https://biaoyansu.com/12.convention) [表/12.convention](https://biaoyansu.com/12.convention) - [JavaScrip…](https://biaoyansu.com/12.x)

安利 • 自动音频

[JS](https://biaoyansu.com/search/JS) [教程](https://biaoyansu.com/search/教程) [干货](https://biaoyansu.com/search/干货) [前端](https://biaoyansu.com/search/前端) [Web开发](https://biaoyansu.com/search/Web开发)

## JS关键词

| 命名        | 意义             | 原型                                                   |
| :---------- | :--------------- | :----------------------------------------------------- |
| `var`       | 变量             | [**var**iable](https://dict.youdao.com/w/eng/variable) |
| `if`        | 如果             | [--](https://dict.youdao.com/w/eng/if)                 |
| `else`      | 否则             | [--](https://dict.youdao.com/w/eng/else)               |
| `return`    | 返回             | [--](https://dict.youdao.com/w/eng/return)             |
| `number`    | 数字             | [--](https://dict.youdao.com/w/eng/number)             |
| `string`    | 字符串           | [--](https://dict.youdao.com/w/eng/string)             |
| `array`     | 数组             | [--](https://dict.youdao.com/w/eng/array)              |
| `object`    | 对象             | [--](https://dict.youdao.com/w/eng/object)             |
| `date`      | 日期             | [--](https://dict.youdao.com/w/eng/date)               |
| `function`  | 函数；方法；功能 | [--](https://dict.youdao.com/w/eng/function)           |
| `null`      | 空（啥都没有）   | [--](https://dict.youdao.com/w/eng/null)               |
| `undefined` | 未定义           | [--](https://dict.youdao.com/w/eng/undefined)          |
| `bool`      | 布尔值           | [**bool**ean](https://dict.youdao.com/w/eng/boolean)   |
| `true`      | 真               | [--](https://dict.youdao.com/w/eng/true)               |
| `false`     | 假               | [--](https://dict.youdao.com/w/eng/false)              |
| `while`     | 当...；只要...   | [--](https://dict.youdao.com/w/eng/while)              |
| `float`     | 浮点数，小数     | [--](https://dict.youdao.com/w/eng/float)              |
| `int`       | 整型，整数       | [**int**eger](https://dict.youdao.com/w/eng/integer)   |
| `in`        | 在...里；内部    | [--](https://dict.youdao.com/w/eng/in)                 |
| `catch`     | 捕获             | [--](https://dict.youdao.com/w/eng/catch)              |
| `delete`    | 删除             | [--](https://dict.youdao.com/w/eng/delete)             |
| `error`     | 错误             | [--](https://dict.youdao.com/w/eng/error)              |

## 常用词

| 命名                 | 意义       | 原型                                               |
| :------------------- | :--------- | :------------------------------------------------- |
| `name`               | 名称，姓名 | [--](https://dict.youdao.com/w/eng/name)           |
| `property`, `prop`   | 属性；特性 | [--](https://dict.youdao.com/w/eng/property)       |
| `method`             | 方法       | [--](https://dict.youdao.com/w/eng/method)         |
| `inherit`            | 继承       | [--](https://dict.youdao.com/w/eng/inherit)        |
| `get`                | 取，取值   | [--](https://dict.youdao.com/w/eng/get)            |
| `set`                | 存，存值   | [--](https://dict.youdao.com/w/eng/set)            |
| `add`                | 加；添加   | [--](https://dict.youdao.com/w/eng/add)            |
| `remove`             | 删除；去除 | [--](https://dict.youdao.com/w/eng/remove)         |
| `update`             | 更新       | [--](https://dict.youdao.com/w/eng/update)         |
| `append`             | 往后追加   | [--](https://dict.youdao.com/w/eng/append)         |
| `prepend`            | 往前追加   | [--](https://dict.youdao.com/w/eng/prepend)        |
| `parent`             | 父级；家长 | [--](https://dict.youdao.com/w/eng/parent)         |
| `child`, `children`  | 孩子       | [--](https://dict.youdao.com/w/eng/child)          |
| `prototype`，`proto` | 原型       | [--](https://dict.youdao.com/w/eng/prototype)      |
| `obj`                | 对象       | [**obj**ect](https://dict.youdao.com/w/eng/object) |
| `index`              | 索引       | [--](https://dict.youdao.com/w/eng/index)          |
| `data`               | 数据       | [--](https://dict.youdao.com/w/eng/data)           |
| `whh`                | 王花花     | - 。-                                              |