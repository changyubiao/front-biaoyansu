# [骚操作](https://biaoyansu.com/12.trick) [表/12.trick](https://biaoyansu.com/12.trick) - [JavaScrip…](https://biaoyansu.com/12.x)

安利 • 自动音频

[JS](https://biaoyansu.com/search/JS) [教程](https://biaoyansu.com/search/教程) [干货](https://biaoyansu.com/search/干货) [前端](https://biaoyansu.com/search/前端) [Web开发](https://biaoyansu.com/search/Web开发)

以下的绝大部分做法的本质是因为懒 ·🐽·

### 判断省略括号

不建议初学者使用，一不小心就坑到自己 _(:з」∠)_

正常版

```js
if(love_me) {
  love_you_too(); // love_me为真才会执行
}
yo(); // 永远都会执行
```

懒癌版

```js
if(love_me)
  love_you_too(); // love_me为真才会执行
yo(); // 永远都会执行
```

掩耳盗铃版

```js
if(love_me)
  love_you_too(); // love_me为真才会执行
  yo(); // 缩进对齐也没用，依然永远都会执行（python表示不服）
```

### 三元运算符

```js
条件 ? 满足时执行 : 否则执行
```

正常版

```js
if(love_me) {
  love_you_too();
} else {
  emmmm();
}
```

懒癌版

```js
love_me ? love_you_too() : emmmm();
```

### 懒癌判断函数

正常版

```js
function greet(love_me) {
  if(love_me) {
    return 'Love you too!';
  } else {
    return 'Emmmm...';
  }
}
```

懒癌版

```js
function greet(love_me) {
  if(love_me) 
    return 'Love you too!'; // 如果love_me为真，此处直接返回，后面的通通不会执行

  return 'Emmmm...'; // 否则就会一直执行到这一步
}
```

### ES5中设置传参默认值

正常版

```js
function 问好(姓名) {
  if(!姓名) {
    姓名 = '王花花';
  }
  return '你好' + 姓名;
}
```

懒癌版

```js
function 问好(姓名) {
  姓名 = 姓名 || '王花花'; // 如果传了姓名就用，没传默认设为"王花花"
  return '你好' + 姓名;
}
```

### 批量声明变量

正常版

```js
var a;
var b;
var c;
```

懒癌版

```js
var a, b, c;
```

懒癌洁癖实用主义综合症患者（逃

```js
var a
  , b
  , c
  ;
```

### 批量变量赋值

正常版

```js
var a = 1;
var b = a;
var c = b;
```

懒癌版

```js
var a = 1, b = a, c = b;
```

懒癌洁癖实用主义综合症患者

```js
var a, b, c;
a = b = c = 1;
```

### 自触发函数 (IIFE)

一般用于隔离作用域，一个文件写一个，防止一不小心污染全局变量。

正常版

```js
启动();

function 启动() {
  'use strict';
  // ...
}
```

懒癌版

```js
(function () {
  'use strict';
  // ...
})();

/*
可以这样理解
先用括号把这个匿名函数包起来：
(匿名函数)

然后执行：
(匿名函数)();
*/
```

懒癌晚期版

```js
!function () {
  'use strict';
  // ...
}();
```

懒癌领悟版

```js
;(function () { // 行首的分号是干嘛的？
  'use strict';
  // ...
})();
```

那个分号是为了给前一个依赖文件擦屁股，js文件压缩时会依次将不同文件连接起来，然后去掉所有空格断行注释之类的东西，这样一来两个文件前后就黏一块了。压缩前好好的，压缩后就跑不起来了？卡了3天，没有任何头绪，难道代码需要呼吸？