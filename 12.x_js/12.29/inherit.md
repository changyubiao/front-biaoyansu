# [JS完整继承关系链](https://biaoyansu.com/12.tree) [表/12.tree](https://biaoyansu.com/12.tree) - [JavaScrip…](https://biaoyansu.com/12.x)

安利 • 自动音频

[JS](https://biaoyansu.com/search/JS) [教程](https://biaoyansu.com/search/教程) [干货](https://biaoyansu.com/search/干货) [前端](https://biaoyansu.com/search/前端) [Web开发](https://biaoyansu.com/search/Web开发)

- `null`

- `undefined`

- `Infinity`

- `NaN`

- `[[原始boolean]]`

- `[[原始number]]`

- `[[原始string]]`

- `Proxy`

- `Symbol`

- ```
  Object
  ```

  - `Number`

  - `String`

  - `Boolean`

  - `Function`

  - `Date`

  - `RegExp`

  - `Array`

  - `Math`

  - `JSON`

  - `ArrayBuffer`

  - `[[各种类Array对象]]`

  - `DataView`

  - `Map`

  - `WeakMap`

  - `Set`

  - `WeakSet`

  - `Promise`

  - `WebAssembly`

  - `Error`

  - `EvalError`

  - `RangeError`

  - `ReferenceError`

  - `SyntaxError`

  - `TypeError`

  - `URIError`

  - `EventTarget`

  - ```
    XMLHttpRequestEventTarget
    ```

    - `XMLHttpRequestUpload`
    - `XMLHttpRequest`

  - `RTCDTMFSender`

  - `RTCDataChannel`

  - `RTCPeerConnection`

  - ```
    Animation
    ```

    - `ShadowAnimation`

  - `WakeLock`

  - `ServiceWorkerContainer`

  - `ServiceWorkerRegistration`

  - `ScreenOrientation`

  - `RemotePlayback`

  - `PresentationConnectionList`

  - `PresentationConnection`

  - `PresentationAvailability`

  - `PresentationRequest`

  - `PermissionStatus`

  - `PaymentRequest`

  - `MediaRecorder`

  - `MediaDevices`

  - ```
    MediaStreamTrack
    ```

    - `CanvasCaptureMediaStreamTrack`

  - `MediaStream`

  - `SourceBufferList`

  - `SourceBuffer`

  - `MediaSource`

  - `IDBTransaction`

  - `IDBDatabase`

  - ```
    IDBRequest
    ```

    - `IDBOpenDBRequest`

  - `Performance`

  - ```
    Sensor
    ```

    - `ProximitySensor`
    - `OrientationSensor`
    - `RelativeOrientationSensor`
    - `AbsoluteOrientationSensor`
    - `UncalibratedMagnetometer`
    - `Magnetometer`
    - `Gyroscope`
    - `AmbientLightSensor`
    - `Accelerometer`
    - `GravitySensor`
    - `LinearAccelerationSensor`

  - `FileReader`

  - `MediaKeySession`

  - `MediaQueryList`

  - `FontFaceSet`

  - `Clipboard`

  - `BatteryManager`

  - `Notification`

  - `SharedWorker`

  - `Worker`

  - ```
    WorkerGlobalScope
    ```

    - `RTCIdentityProviderGlobalScope`
    - `ServiceWorkerGlobalScope`
    - `SharedWorkerGlobalScope`
    - `DedicatedWorkerGlobalScope`

  - `BroadcastChannel`

  - `MessagePort`

  - `WebSocket`

  - `EventSource`

  - `ApplicationCache`

  - `Window`

  - `OffscreenCanvas`

  - ```
    TextTrackCue
    ```

    - `VTTCue`

  - `TextTrack`

  - `TextTrackList`

  - `VideoTrackList`

  - `AudioTrackList`

  - ```
    Node
    ```

    - `CharacterData`

    - `Comment`

    - `ProcessingInstruction`

    - ```
      Text
      ```

      - `CDATASection`

    - `Attr`

    - `Element`

    - ```
      SVGElement
      ```

      - `SVGViewElement`

      - `SVGScriptElement`

      - `SVGCursorElement`

      - `SVGHatchpathElement`

      - `SVGHatchElement`

      - `SVGPatternElement`

      - `SVGStopElement`

      - `SVGMeshpatchElement`

      - `SVGMeshrowElement`

      - `SVGGradientElement`

      - `SVGMeshGradientElement`

      - `SVGRadialGradientElement`

      - `SVGLinearGradientElement`

      - `SVGSolidcolorElement`

      - `SVGMarkerElement`

      - `SVGStyleElement`

      - `SVGTitleElement`

      - `SVGMetadataElement`

      - `SVGDescElement`

      - `SVGGraphicsElement`

      - `SVGAElement`

      - `SVGForeignObjectElement`

      - `SVGImageElement`

      - ```
        SVGTextContentElement
        ```

        - `SVGTextPathElement`
        - `SVGTextPositioningElement`
        - `SVGTSpanElement`
        - `SVGTextElement`

      - `SVGSwitchElement`

      - `SVGUseElement`

      - `SVGSymbolElement`

      - `SVGDefsElement`

      - `SVGUnknownElement`

      - `SVGGElement`

      - `SVGSVGElement`

      - ```
        SVGGeometryElement
        ```

        - `SVGPolygonElement`
        - `SVGPolylineElement`
        - `SVGMeshElement`
        - `SVGLineElement`
        - `SVGEllipseElement`
        - `SVGCircleElement`
        - `SVGRectElement`
        - `SVGPathElement`

    - ```
      HTMLElement
      ```

      - `HTMLFontElement`
      - `HTMLDirectoryElement`
      - `HTMLFrameElement`
      - `HTMLFrameSetElement`
      - `HTMLMarqueeElement`
      - `HTMLAppletElement`
      - `HTMLCanvasElement`
      - `HTMLSlotElement`
      - `HTMLTemplateElement`
      - `HTMLScriptElement`
      - `HTMLDialogElement`
      - `HTMLDetailsElement`
      - `HTMLLegendElement`
      - `HTMLFieldSetElement`
      - `HTMLMeterElement`
      - `HTMLProgressElement`
      - `HTMLOutputElement`
      - `HTMLTextAreaElement`
      - `HTMLOptionElement`
      - `HTMLOptGroupElement`
      - `HTMLDataListElement`
      - `HTMLSelectElement`
      - `HTMLButtonElement`
      - `HTMLInputElement`
      - `HTMLLabelElement`
      - `HTMLFormElement`
      - `HTMLTableCellElement`
      - `HTMLTableRowElement`
      - `HTMLTableSectionElement`
      - `HTMLTableColElement`
      - `HTMLTableCaptionElement`
      - `HTMLTableElement`
      - `HTMLAreaElement`
      - `HTMLMapElement`
      - `HTMLMediaElement`
      - `HTMLTrackElement`
      - `HTMLParamElement`
      - `HTMLObjectElement`
      - `HTMLEmbedElement`
      - `HTMLIFrameElement`
      - `HTMLImageElement`
      - `HTMLSourceElement`
      - `HTMLPictureElement`
      - `HTMLModElement`
      - `HTMLBRElement`
      - `HTMLSpanElement`
      - `HTMLTimeElement`
      - `HTMLDataElement`
      - `HTMLAnchorElement`
      - `HTMLDivElement`
      - `HTMLDListElement`
      - `HTMLLIElement`
      - `HTMLMenuElement`
      - `HTMLUListElement`
      - `HTMLOListElement`
      - `HTMLQuoteElement`
      - `HTMLPreElement`
      - `HTMLHRElement`
      - `HTMLParagraphElement`
      - `HTMLHeadingElement`
      - `HTMLBodyElement`
      - `HTMLStyleElement`
      - `HTMLMetaElement`
      - `HTMLLinkElement`
      - `HTMLBaseElement`
      - `HTMLTitleElement`
      - `HTMLHeadElement`
      - `HTMLHtmlElement`
      - `HTMLUnknownElement`

    - `DocumentFragment`

    - ```
      ShadowRoot
      ```

      - `SVGUseElementShadowRoot`

    - `DocumentType`

    - `Document`

    - `XMLDocument`