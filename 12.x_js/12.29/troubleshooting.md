
# [常见错误](https://biaoyansu.com/12.troubleshooting) [表/12.troubleshooting](https://biaoyansu.com/12.troubleshooting) - [JavaScrip…](https://biaoyansu.com/12.x)

安利 • 自动音频

[JS](https://biaoyansu.com/search/JS) [教程](https://biaoyansu.com/search/教程) [干货](https://biaoyansu.com/search/干货) [前端](https://biaoyansu.com/search/前端) [Web开发](https://biaoyansu.com/search/Web开发)

- Uncaught ReferenceError:

  a is not defined

  - 解释：`a`未定义。
  - 解决方案：一般情况下是敲错变量名，否则先定义`a`再使用它。
  


- Uncaught TypeError:

  Cannot read property 'a' of undefined

  - 解释：`a`的父级未定义。
  - 解决方案：一般情况下是敲错属性链；要确保`a`的父级是个对象。

- Uncaught TypeError:

  a is not a function

  - 解释：`a`不是个函数，而你想触发它。