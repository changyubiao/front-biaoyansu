

var isAdmin = true;


if (isAdmin){
	console.log('他是管理员');
}else{
	console.log('他不是管理员');
}


/*

常见类型的 真值 
*/ 


	 	
// false
// 0 
// ''
// NaN
// null
// undefined
// // 这些 都是 false


// // Number
// !!0 // false 
// !!1 // true 
// !!-1 // true
// !!0.001 // true



// // String
// !!'' // false
// !!'yo' // true
// !!'0' // true

// // 其他
// !!null // false
// !!undefined // false
// !!NaN // false
// !!function() {} // true
// !![] // true
// !!{} // true




// !!0 // false
// !!'' // false