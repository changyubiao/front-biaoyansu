var arr = [1, 2, 3, 4, 5, 10, 15, 20];


function each(arr, callback) {

	for (var i = 0; i < arr.length; i++) {
		item = arr[i];
		if (callback) {
			callback(item);
		}

	}
}


// callback 函数的作用 ，可以自定义 如何打印，item ,
// 打印条件可以在 callback 中定义， 不用修改 each 函数。就相当于在 each 中 打开了一个口子 交给 callback ，callback 如何使用 这个item,就属于 callback 的实现，和主要逻辑不冲突。 这样就实现了 功能的分离。遍历 + 打印数组 分离开了。
function equal_gt_ten(item) {
	if (item > 10) {
		console.log("item:", item);
	}
}


// 调用 + callback
each(arr, equal_gt_ten);


// 调用 传入匿名函数
each(arr,function(item){
	if (item <3){
		console.log(item);
	}
	
})


each(arr,function(item){
	if (item ==5){
		console.log(item);
	}
})

