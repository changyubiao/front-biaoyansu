# javascript精讲

[javascript精讲课程地址](https://biaoyansu.com/12.1)


## 12.1 前置工作
## 12.2 基本语法
## 12.3 数据类型
## 12.4 变量
## 12.5 控制流：if & else 以及真值判断
## 12.6 控制流 Switch
## 12.7 运算符
## 12.8 循环 for loop
## 12.9 循环 while loop 
## 12.10 函数
## 12.11 闭包
[闭包课程地址](https://biaoyansu.com/12.11)
## 12.12  windows 对象 alert confirm和prompt
## 12.13 window对象: setTimeout setInterval和clearInterval
## 12.14 Number - 数据类型详解
## 12.15 String - 数据类型详解
## 12.16 Object - 数据类型详解
## 12.17 Array - 数据类型详解
## 12.18 null - 数据类型详解
## 12.19 undefined - 数据类型详解
## 12.20  原型 -构造器
## 12.21  原型 prototype 和 \_\_proto__
## 12.22 原型 - 原生对象的原型
## 12.23 原型 - 多级继承链怎么实现
## 12.24 原型 总结
## 12.25 this - 是个啥
## 12.26 this - this - call, apply, bind 
## 12.27 回调函数 - 是个啥
## 12.28  回调 怎么用
## 12.29 documents JS关键词