// exp1. 创建对象 
// var a = {'name':'a'};
// var b = new Object();
// console.log(a);
// console.log(b);
// console.log(a.constructor  == b.constructor);




/*
创建一个 非常干净的对象 
*/
// 这里不纯净
// var a = new Object();
// console.log(a);


// 里面啥都没有啦
// 这里的意思 以 null 作为原型
var b = Object.create(null);
console.log(b);



// 以 {a:1,name:'frank'}为原型创建对象 
// var b = Object.create({a:1,name:'frank'});
// console.log(b);




//  a.__proto__  的 constructor  是 Array 
var a= [];
// var b = new Array();
console.log(a);

