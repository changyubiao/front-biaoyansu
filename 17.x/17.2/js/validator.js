$(function() {

	"use strict";

	/**
	 *  构造函数  构造器 
	 * @param {String} value 
	 * @param {Object} rule 
	 */
	window.Validator = function(value, rule) {
		/**
		 * 函数描述 validate_xxxx
		 * 验证器 根据规则 验证是否 value 可以通过该 规则，如果可以满足规则  返回true 
		 * 否则返回false 
		 * @param {string} value 参数1的说明
		 * @param {Object} rule 参数2的说明，比较长
		 *  
		 * @return {Boolean} 返回值描述
		 */
		this.validate_min = function() {
			var real_value = pre_value(value);
			return real_value >= rule.min;
		};

		this.validate_max = function() {
			var real_value = pre_value(value);
			return real_value <= rule.max;
		};



		/**
		 * validate_min
		 * validate_max 对value 进行前置的处理工作
		 *  
		 * @param {value} value 
		 */
		var pre_value = function(value) {
			return parseFloat(value);
		}


		this.validate_maxlength = function() {

			var real_value = pre_length();
			return real_value.length <= rule.maxlength;


		};

		this.validate_minlength = function() {
			var real_value = pre_length();
			return real_value.length >= rule.minlength;

		};


		function pre_length() {
			return value.toString().trim();
		}


		this.validate_numeric = function() {
			return $.isNumeric(value) == rule.numeric;
		};

		this.validate_required = function() {
			var real = $.trim(value);
			if (!real && real != 0) {
				return false;
			}
			return true;

		};

		// 验证 正则表达式
		this.validate_pattern = function() {
			var pattern = RegExp(rule.pattern);
			return pattern.test(value);
		}

	};

});
