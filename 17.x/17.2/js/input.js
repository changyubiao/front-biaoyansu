$(function() {



	"use strict";



	window.Input = function(selector) {
		var $ele;

		var rule = {

			required: true
		};



		function init() {
			find_ele();
			parse_rule();
			this.load_validator();

		}


		this.get_val = function() {
			return $ele.val();
		}

		this.load_validator = function() {
			var value = this.get_val();
			// 绑定到this 上面
			this.validator = new Validator(value, rule);
		}


		function parse_rule() {
			// "max:10|min:2|maxlength:20:minlength:2|required:true|numeric:false"
			var rule_str = $ele.data('rule');

			if (!rule_str) {
				return
			}
			let key;
			let val;

			var rule_arr = rule_str.split('|')
			for (var i = 0; i < rule_arr.length; i++) {
				[key, val] = rule_arr[i].split(':');
				rule[key] = JSON.parse(val);
			}

			// console.log(rule);
		}

		function find_ele() {
			if (selector instanceof jQuery) {
				$ele = selector;
			} else {
				$ele = $(selector);
			}

		}


		// call init  method 
		init();
	}





});
