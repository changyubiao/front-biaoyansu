$(function() {
	"use strict";

	console.log('main.js begin');

	/*
	1 选中所有的input data-rule 的input  
	
	2. 解析规则 parse  rule 
	
	3. valiate   Validator  
		valid    invalid 
		
		
	// var input = new Input('#test');
	// var valid = input.validator.is_valid();
	// var inputs = new Input('input[data-rule]');
	
	*/




	var $inputs = $('input[data-rule]');
	var inputs = [];
	var $form = $('#signup');

	$inputs.each(function(index, node) {
		console.log(index, node);
		/*解析每一个input的验证规则*/
		inputs.push(new Input(node));
	})


	// 提交 注册
	$form.on('submit', function(e) {
		// 阻止 默认行为
		e.preventDefault();
		$inputs.trigger('blur');

		for (var i = 0; i < inputs.length; i++) {
			var item = inputs[i];
			var r = item.validator.is_valid();
			if (!r) {
				alert('invalid,注册信息填写有误,请注意核查!');
				return;
			}
		}

		alert('注册成功');
	})


	// 发送请求 到服务器
	function signup() {
		// call  backend api 
		// $.post('/api/signup', {...})
	}


});




function test_validator() {


	var validator = new Validator('', {
		min: 5,
		max: 50,
		maxlength: 10,
		minlength: 2,
		numeric: true,
		required: false,
		pattern: '^[a-z]*$'
	})

	// 'pattern:"[0-9]*"|max:10|min:2|maxlength:20:minlength:2|required:true|numeric:false'

	// console.log('validator ',validator);
	// var valid = validator.validate_min();
	// console.log('valid:', valid);


	// var valid = validator.validate_max();
	// console.log('valid:', valid);


	// var valid = validator.validate_minlength();
	// console.log('valid:', valid);


	// var valid = validator.validate_maxlength();
	// console.log('valid:', valid);


	// var valid = validator.validate_numeric();
	// console.log('valid:', valid);

	var valid = validator.validate_required();
	console.log('valid:', valid);

	// var valid = validator.validate_pattern();
	// console.log('valid:', valid);


}
