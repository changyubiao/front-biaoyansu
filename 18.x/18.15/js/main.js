/*

https://cn.vuejs.org/v2/guide/filters.html

*/




// 全局定义指令 
Vue.directive('pin', function (el, binding) {

	var pinned = binding.value;
	console.log("pinned:", pinned);
	// console.log("el:", el);

	if (pinned) {
		el.style.position = 'fixed';
		el.style.top = '10px';
		el.style.left = '10px';
	} else {
		el.style = 'static';
	}
})



// 定义一个域
var app = new Vue({
	el: '#app',
	data: {
		card1: {
			pinned: false
		},
		card2: {
			pinned: false
		},

	},

	methods: {
		on_click_card1: function () {
			this.card1.pinned = !this.card1.pinned;
		},

		on_click_card2: function () {
			this.card2.pinned = !this.card2.pinned;
			console.log('this.card2:' ,this.card2.pinned);
		}
	},


	// 这个组件 只在这个域里 使用 
	components: {

	},



	// 定义过滤器,只在这个域里面使用
	filters: {
		capitalize: function (value) {
			if (!value) {
				return ''
			}
			value = value.toString()
			return value.charAt(0).toUpperCase() + value.slice(1);
		}
	}
})


