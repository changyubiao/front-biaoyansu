/*

过滤器
https://cn.vuejs.org/v2/guide/filters.html




指令
https://cn.vuejs.org/v2/guide/custom-directive.html

指令的修饰符，以及传参


18.17  mixins 
https://cn.vuejs.org/v2/guide/mixins.html




18.18 插槽 
https://cn.vuejs.org/v2/guide/components-slots.html
*/



// panel组件 
Vue.component('panel', {

	template: '#panel-tpl',

	data: function () {
		return { visible: true }
	},

})






// 定义一个域
var app = new Vue({
	el: '#app',
	data: {

	},

	methods: {

	},


	// 这个组件 只在这个域里 使用 
	components: {

	},




})


