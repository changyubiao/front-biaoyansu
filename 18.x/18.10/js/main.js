

/*
https://cn.vuejs.org/v2/guide/components.html


全局都能用

*/


// 1. 全局使用
// Vue.component('button-like', {

// 	template: '<button @click="on_click">  <img src="img/like-icon.webp" alt=""> {{like}}</button>',

// 	data: function () {
// 		return {
// 			like: 0,
// 			is_clicked: false
// 		};
// 	},

// 	methods: {

// 		on_click: function () {
// 			console.log('click button ...')
// 			if (!this.is_clicked) {
// 				this.like++;
// 			} else {
// 				this.like = 0;
// 			}
// 			this.is_clicked = !this.is_clicked;
// 		}

// 	}

// })



// 定义一个组件 内容 
let button_like = {
	// 这里要是 一个id 选择器 
	template: '#button-like-tpl',
	data: function () {
		return {
			like: 10,
			is_clicked: false
		};
	},

	methods: {
		on_click: function () {
			if (!this.is_clicked) {
				this.like++;
			} else {
				this.like--;
			}
			this.is_clicked = !this.is_clicked;
		}

	}
}


// 定义一个域
var app = new Vue({
	el: '#app',
	data: {

	},
	// 这个组件 只在这个域里 使用 
	components: {
		'button-like': button_like
	}
})

