

/*
https://cn.vuejs.org/v2/guide/components.html
全局都能用

*/


// 定义一个事件 
var Event = new Vue();


// 1. 娟儿组件
Vue.component('juan-er', {

	template: `
		<div>
		娟儿说: <input  @keyup="onchange"  v-model="i_said">
		</div>		
	`,

	data: function () {
		return {
			i_said: ''
		}
	},

	methods: {
		onchange: function () {
			// 发送一个监听, 并且发送 说的内容，到第二个参数。
			Event.$emit('huhua-said-something', this.i_said);
		}

	}

});


// 2.  frank 组件 
Vue.component('frank', {

	// html字符串
	template: `
		<div> 
		frank听:<input  v-model="huahua_said">
		</div>
	`,

	data: function () {
		return {
			huahua_said: '',
		}
	},
	methods: {

	},

	// 挂载完毕 的时候
	mounted: function () {
		var me = this;
		// console.log('me:', me);
		// 监听这个事件, 发送的事件
		Event.$on('huhua-said-something', function (sentence) {
			// console.log('sentence:', sentence);
			// console.log('this:', this);
			me.huahua_said = sentence;
		})

	},

});



// 定义一个域
var app = new Vue({
	el: '#app',
	data: {

	},
	// 这个组件 只在这个域里 使用 
	components: {

	}
})

