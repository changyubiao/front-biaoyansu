var app = new Vue({
	el: '#app',
	data: {

		math: 90,
		english: 40,
		physics: 79,
	},

	methods: {
		round: function(number, precision) {
			return Math.round(+number + 'e' + precision) / Math.pow(10, precision);
		},
	},



	// 计算属性 computed property
	computed: {

		sum: function() {
			return this.math + this.physics + this.english

		},
		avg: function() {
			// 直接可以用 之前计算的结果 sum;this.sum 
			return this.round(this.sum / 3, 2);

		},


	}
})
