

/*
https://cn.vuejs.org/v2/guide/components.html


全局都能用

*/


// 1. template 传递参数
Vue.component('user', {

	template: `<a :href="/user/+ username" >{{username}}</a>`,

	// 给标签 属性绑定数据 
	props: ['username'],
	methods: {

	}

});



// 定义一个域
var app = new Vue({
	el: '#app',
	data: {

	},
	// 这个组件 只在这个域里 使用 
	components: {

	}
})

