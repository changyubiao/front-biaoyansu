


var app = new Vue({
	el: '#app',
	data: {
		name: "frank",
		// fruits: ['apple', "orange", "pear", "banana"],
		fruits: [

			{
				name: 'apple',
				price:18.2,
				id:1,
				discount:0.2


			},

			{
				name: 'orange',
				price:18.3,
				id:2,
				discount:0.5

			},

			{
				name: 'pear',
				price:10,
				id:3,
				// discount:0.4
			},
			{
				name: 'banana',
				price:12.2,
				id:4,
				discount:0.8

			}
		]
	}
})
