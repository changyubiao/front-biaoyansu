/*

过滤器
https://cn.vuejs.org/v2/guide/filters.html




指令
https://cn.vuejs.org/v2/guide/custom-directive.html

指令的修饰符，以及传参


18.17  mixins 
https://cn.vuejs.org/v2/guide/mixins.html


*/


var base = {

	data: function () {
		return {
			visible: false
		}

	},
	methods: {

		show: function () {
			this.visible = true;
		},
		hide: function () {
			this.visible = false;
		},

		toggle: function () {
			this.visible = !this.visible;

		}


	}


}



// 提示框组件 
Vue.component('tooltip', {

	template: `
	<div>

		<span @mouseenter="show"  @mouseleave="hide">bye bye</span>

		<div v-if="visible">
			Frank is a student.
		</div>

	</div>
	`,

	mixins: [base],

	data: function () {
		return { visible: true }
	},

})


// 弹出层
Vue.component('popup', {

	template: `
	<div>
		<button @click="toggle">Popup</button>
		<div v-if="visible">
			<span @click="hide">X</span>
			<h4> title </h4>
			Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas numquam, mollitia quod commodi
			excepturi
			facere doloremque doloribus quo, id repellat corporis, similique enim distinctio! Repellat porro tenetur
			illo provident enim.
		</div>
	</div>
	`,

	methods: {
	},

	mixins: [base],
})





// 定义一个域
var app = new Vue({
	el: '#app',
	data: {

		visible: false
	},

	methods: {

	},


	// 这个组件 只在这个域里 使用 
	components: {

	},




})


