

/*
https://cn.vuejs.org/v2/guide/components.html


全局都能用

*/


// 1. 父组件 
Vue.component('banlance', {

	// 第一种定义template 
	// template: `
	// <div>
	// 	<show @show-balance="show_banlance" ></show>
	// 	<div v-if="is_show">
	// 		你的余额为: {{balance}}
	// 	</div>

	// </div>
	// `,

	// 第二种
	template: '#banlance-tpl',

	data: function () {
		return {
			is_show: false,
			balance: 0
		}
	},

	methods: {
		get_banlance: function (data) {
			// data 用来接收 子组件 传递来的数据，当然也可以不传
			console.log('data:', data);
			this.is_show = true;

			// 如果没有data 就使用默认值。
			if (data && data.balance) {
				// 修改 data 里面的数据
				this.balance = data.balance;
			}

		}
	}

});


// 2. 子组件  传递数据 到父组件
Vue.component('show', {

	// 注意这里可以传 id选择器，或者 html字符串
	template: '#show-balance-tpl',

	methods: {
		on_click: function () {
			// 触发一个事件,并且传递了数据，给父组件 
			this.$emit('show-balance', { a: 10, balance: 10.9 });
			// this.$emit('show-balance');
		}
	}

});



// 定义一个域
var app = new Vue({
	el: '#app',
	data: {

	},
	// 这个组件 只在这个域里 使用 
	components: {

	}
})

