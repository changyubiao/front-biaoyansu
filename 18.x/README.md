# 18.x Vue.js精讲 - 表严肃讲Vue.js

[18.x Vue 精讲](https://biaoyansu.com/18.x)



## 目录
## 18.0 环境搭建
## 18.1 引入vue 以及基础语法
## 18.2 vue v-for 指令
## 18.3 v-bind 指令
## 18.4 v-on 指令 时间绑定
## 18.5 v-model 指令以及修饰符
## 18.6 v-model 指令 其他元素
## 18.7 v-if 语句
## 18.8 计算属性
## 18.9 组件 全局及局部
## 18.10 组件配置
## 18.11 父子组件通信 
## 18.12 父子组件通信-子与父通信
## 18.13 18.13 组件通信-兄弟间组件通信 
## 18.14 filter 过滤器
## 18.15 自定义指令基础 
## 18.16 自定义指令 基础2 配置传参以及修饰符
## 18.17 混合minxs
## 18.18 slot 插槽
## cookbook 简单演示

## demo 组件兄弟间的通信

---
## 参考文档

[Vue.js 教程-菜鸟教程](https://www.runoob.com/vue2/vue-tutorial.html)
[Vue filter ]([18.1/demo.html](https://cn.vuejs.org/v2/guide/filters.html))

[Vue filter](https://cn.vuejs.org/v2/guide/filters.html)

[Vue directive 指令](https://cn.vuejs.org/v2/guide/custom-directive.html)
[Vue mixins ](https://cn.vuejs.org/v2/guide/mixins.html)
