// 定义一个事件 
var Event = new Vue();


// 1. 你组件
Vue.component('you', {
	template: '#you-tpl',

	data: function() {
		return {
			something: ''
		}
	},
	methods: {
		onchange: function() {
			this.something = this.capitalize(this.something);
			// 发送一个监听, 并且发送 说的内容，第二个参数。
			Event.$emit('you-said-something', this.something);
		},

		
		capitalize: function(value) {
			if (!value) {
				return ''
			}
			value = value.toString()
			return value.charAt(0).toUpperCase() + value.slice(1);
		}

	}

});


// 2. frank 组件 
Vue.component('frank', {
	template: '#me-tpl',

	data: function() {
		return {
			something: '',
		}
	},

	// 挂载完毕 的时候
	mounted: function() {
		var me = this;
		// 监听这个事件, 发送的事件
		Event.$on('you-said-something', function(sentence) {
			me.something = sentence;
		})
	},

});



// 定义一个域
var app = new Vue({
	el: '#app',
	data: {},
});
