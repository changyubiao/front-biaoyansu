

/*
https://cn.vuejs.org/v2/guide/components.html


全局都能用

*/


// 1. 全局使用
// Vue.component('button-counter',{} )




// 定义一个 Component 
var button_counter = {
	// 注意这里data 是一个function 
	// 一个组件的 data 选项必须是一个函数
	data: function () {
		return { count: 0 };
	},
	// 组件的内容
	template: '<button @click="on_click">You clicked me {{ count }} times.</button>',
	methods: {
		on_click: function () {
			// console.log(this);
			this.count++;
		}
	}

};


// 定义一个域
var app = new Vue({
	el: '#seg1',
	data: {

	},

	// 这个组件 只在这个域里 使用 
	components: {
		'button-counter': button_counter
	}

})


var app2 = new Vue({
	el: '#seg2',

})