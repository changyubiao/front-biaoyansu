; (function () {
	'use strict';



	// 全局定义 过滤器 [Vue warn]: Failed to resolve filter: curracy
	Vue.filter('curracy', function (val, unit) {
		unit = unit || '元'
		return (parseFloat(val)).toFixed(2) + unit
	});


	let app = new Vue({
		el: '#app',
		data: {
			keyword: '西游记',
			result: {},
		},
		methods: {
			// 搜索api 
			search: function () {
				var me = this;
				$.ajax({
					url: 'http://192.168.1.7:5000/search/books?kw=' + me.keyword,
					dataType: 'json',
				}).then(function (r) {
					me.result = r;
				})
			}
		},


	});
})();

