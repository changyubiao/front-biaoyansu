/*

过滤器
https://cn.vuejs.org/v2/guide/filters.html




指令
https://cn.vuejs.org/v2/guide/custom-directive.html

指令的修饰符，以及传参

*/




// 全局定义指令 
Vue.directive('pin', function (el, binding) {

	var pinned = binding.value;
	// console.log("pinned:", pinned);
	var position = binding.modifiers;


	// 获取参数 
	var arg = binding.arg;
	// console.log("position:", position);
	if (pinned) {
		el.style.position = 'fixed';

		for (let key in position) {
			if (key) {
				el.style[key] = '10px'
			}
		}
		if (arg === 'warning') {
			el.style['background-color'] = 'yellow';
		}
	} else {
		el.style = 'static';
	}
})



// 定义一个域
var app = new Vue({
	el: '#app',
	data: {
		card1: {
			pinned: false
		},
		card2: {
			pinned: false
		},

	},

	methods: {
		on_click_card1: function () {
			this.card1.pinned = !this.card1.pinned;
		},

		on_click_card2: function () {
			this.card2.pinned = !this.card2.pinned;
			console.log('this.card2:', this.card2.pinned);
		}
	},


	// 这个组件 只在这个域里 使用 
	components: {

	},



	// 定义过滤器,只在这个域里面使用
	filters: {
		capitalize: function (value) {
			if (!value) {
				return ''
			}
			value = value.toString()
			return value.charAt(0).toUpperCase() + value.slice(1);
		}
	}
})


