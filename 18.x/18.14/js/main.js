

/*

https://cn.vuejs.org/v2/guide/filters.html

*/



// 全局定义
Vue.filter('curracy', function (val, format) {
	format = format || '元'
	return (parseFloat(val / 1000)).toFixed(2) + format
})



// 定义一个域
var app = new Vue({
	el: '#app',
	data: {
		money: 10,


		name: 'frank is a student.'

	},
	// 这个组件 只在这个域里 使用 
	components: {

	},


	// 定义过滤器,只在这个域里面使用
	filters: {
		capitalize: function (value) {
			if (!value) {
				return ''
			}
			value = value.toString()
			return value.charAt(0).toUpperCase() + value.slice(1);
		}
	}
})




var app2 = new Vue({
	el: '#segment2',
	data: {
		money: 1000,

		name: 'rabbit is a student.'
	}
})