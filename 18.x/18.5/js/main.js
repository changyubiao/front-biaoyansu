


var app = new Vue({
	el: '#app',
	data: {
		name: "frank",
		// url:'http://www.baidu.com',
		url: '#',
		img: 'https://dummyimage.com/600x400/e3d7e3/151fad',
		klass: 'btn btn-primary',
		isActive: true,

		price: 10,
	},

	methods: {

		onClick: function () {
			console.log('Clicked ...');
		},

		onEnter: function () {
			console.log('Mouse enter...');
		},

		onOut: function () {
			console.log('Mouse leave...');
		},

		onSubmit: function (event) {
			// 阻止默认行为
			// event.preventDefault();
			console.log('form submitted...');
		},

		onEnter: function () {

			console.log('enter pressed');
		}
	}

})
