// myStorage = localStorage;


;
(function() {

	function set(key, val) {
		let val2 = JSON.stringify(val);
		localStorage.setItem(key, val2);
	}


	/**
	 * 
	 * @param {string} key 
	 * @param {string} default_val 
	 */
	function get(key, default_val) {
		var val = localStorage.getItem(key);
		if (!default_val) {
			default_val = '[]';
		}
		if (!val) {
			val = default_val;
		}
		return JSON.parse(val);
	}

	function clear() {
		localStorage.clear();
	}


	// 暴露接口 绑定在window 对象上面
	window.ms = {
		set: set,
		get: get,
		clear: clear
	}


})();



// ms.set('name','frank');

// console.log(ms.get('name'));



// ms.set('mylist',[1,2,3]);

// console.log(ms.get('mylist'));
