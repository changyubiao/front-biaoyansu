// 事件
var Event = new Vue();


// 定义一个组件 task
Vue.component('task', {

	template: '#task-tpl',

	props: ['todo'],

	methods: {
		action: function(name, params) {
			Event.$emit(name, params);
		}
	}

})


//  2020-03-05T13:30
// remind_at: "2022-05-12T08:19"

var app = new Vue({
	el: '#app',
	data: {
		list: [],
		id: 0,

		current: {
			title: '',
			completed: false,
			desc: '....',
			remind_at: '',
			confirmed: false,
		},


	},




	// el 被新创建的 vm.$el 替换, 挂在成功	
	mounted: function() {
		// console.log('mounted...')
		this.list = ms.get('list');
		// console.log(this.list);

		var me = this;

		var default_remind_at = new Date().format("yyyy-MM-ddThh:mm")
		this.current.remind_at = default_remind_at;



		// 根据父组件的消息 来删除数据
		Event.$on('remove', function(id) {
			// console.log('params:', id);
			me.remove(id);
		});


		// 根据父组件的消息 来更新数据
		Event.$on('set_current', function(todo) {
			// console.log('params:', todo);
			me.set_current(todo);
		});


		// 根据父组件的消息 来切换状态
		Event.$on('complete_toggle', function(id) {
			// console.log('params:', id);
			me.complete_toggle(id);
		});


		// me.check_reminds();

		setInterval(function() {
			me.check_reminds();
		}, 5000)

	},


	methods: {


		check_reminds: function() {

			var me = this;

			me.list.forEach(function(item, i) {

				var remind_at = item.remind_at;

				if (!remind_at || item.confirmed) {
					return
				}


				// 获取时间戳 毫秒
				var remind_timestamp = (new Date(remind_at)).getTime();

				// 当前时间
				var now = (new Date()).getTime();
				
				// 当前时间超过了 提醒时间 才进行提醒
				if (now >= remind_timestamp) {
					// console.log('需要提醒,item:', item);
					var confirmed = confirm(item.title);
					console.log("confirmed:", confirmed);
					if (confirmed) {
						Vue.set(me.list[i], 'confirmed', confirmed);
						console.log(me.list[i]);
					}

				} else {
					// console.log('不需要提醒,item:', item);
				}
			})


		},


		// copy obj 对象 
		deepcopy: function(obj) {
			var copyed = Object.assign({}, obj);
			return copyed
		},


		find_index: function(id) {
			var index = this.list.findIndex(function(item) {
				return item.id === id;
			});
			return index;
		},

		// 从list 获取当前 最大的id 
		find_list_max_id: function() {

			// var cur_id = Math.max.apply(Math, this.list.map(function (item) { return item.id }))
			// if (Math.abs(cur_id) === Infinity) {
			// 	return 0;
			// }
			// return cur_id;

			let n = this.list.length;
			if (!n) {
				return n;
			}
			return this.list[n - 1].id;
		},



		merge: function() {
			// console.log('add a plan ..');
			var is_updated, id;
			is_updated = id = this.current.id;
			if (is_updated !== undefined) {
				this.update(id);
			} else {
				this.add();
			}
			// plan 计划表 清空
			this.reset_current();

		},

		// 添加一个 plan
		add: function() {
			var title = this.current.title;
			console.log('this.list', this.list);
			if (!title) {
				console.log('no title');
			} else {
				var todo = this.deepcopy(this.current);
				// 自己生成一个id 
				todo.id = this.next_id();
				console.log('todo:', todo);
				this.list.push(todo);
			}

		},

		remove: function(id) {
			var index = this.find_index(id);
			this.list.splice(index, 1);
			// console.log(this.list);
		},

		update: function(id) {
			// console.log('update begin.. id:', id);
			// console.log('updated id:', id);
			// 拿到当前要更新的索引
			var index = this.list.findIndex(function(item) {
				return item.id === id;
			});
			// console.log('index:', index);
			// update plan 
			var copyed = this.deepcopy(this.current);
			// Vue更新 list 方法 
			Vue.set(this.list, index, copyed);
		},

		set_current: function(todo) {
			// 注意这里copy 
			this.current = this.deepcopy(todo);
			// this.current.confirmed = false;
		},

		reset_current: function() {
			this.set_current({})
		},


		complete_toggle: function(id) {
			var i = this.find_index(id);
			// console.log("complete_toggle: i:",i);
			Vue.set(this.list[i], 'completed', !this.list[i].completed);
		},



		/*
		 设置一个自增id 
		*/
		next_id: function() {
			// n = this.list.length;
			let n = this.find_list_max_id();
			this.id = ++n;
			return this.id;
		},

	},


	watch: {
		list: {
			deep: true,
			handler: function(new_list, old) {
				// console.log('watch execute... new_list:', new_list);
				if (new_list) {
					ms.set('list', new_list);
				} else {
					ms.set('list', []);
				}

			}
		}
	}

});




// todo  
;
(function() {


})();
