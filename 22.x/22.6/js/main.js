

var app = new Vue({
	el: '#app',
	data: {
		list: [],
		id: 0,

		current: {
			title: '',
			completed: false,
			desc: '....',
			remind_at: '2021-10-01',
		}
	},



	methods: {

		// copy obj 对象 
		deepcopy: function (obj) {
			var copyed = Object.assign({}, obj);
			return copyed
		},




		merge: function () {
			// console.log('add a plan ..');
			var is_updated, id;
			is_updated = id = this.current.id;
			if (is_updated) {
				this.update(id);
			} else {
				this.add();
			}
			// plan 计划表 清空
			this.reset_current();

		},

		// 添加一个 plan
		add: function () {
			var title = this.current.title;
			if (!title) {
				console.log('no title');
			} else {
				var todo = Object.assign({}, this.current);
				// 自己生成一个id 
				todo.id = this.next_id();
				// console.log(todo);
				this.list.push(todo);
			}

		},

		remove: function (id) {
			this.list.splice(id, 1);
			// console.log(this.list);
		},

		update: function (id) {
			// console.log('update begin.. id:', id);
			// console.log('updated id:', id);
			// 拿到当前要更新的索引
			var index = this.list.findIndex(function (item) {
				return item.id === id;
			});
			// console.log('index:', index);
			// update plan 
			// var copyed = Object.assign({}, this.current);
			var copyed = this.deepcopy(this.current);
			// Vue更新 list 方法 
			Vue.set(this.list, index, copyed);
		},

		set_current: function (todo) {
			// 注意这里copy 
			this.current = this.deepcopy(todo);
		},

		reset_current: function () {
			this.set_current({})
		},

		/*
		 设置一个自增id 
		*/
		next_id: function () {
			this.id++;
			return this.id;
		}

	}

});




// todo  
; (function () {


})();



/*


方法使用:
Object.assign({},this.current);

*/
