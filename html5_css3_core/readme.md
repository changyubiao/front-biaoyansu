

# HTML5 与CSS3 核心技法





## roadmap 
[roadmap](https://biaoyansu.com/#roadmap)

## css 入门实战首页
[css 入门实战首页](https://biaoyansu.com/9.x)

## 课程代码
github.com/biaoyansu/课程ID  
如: github.com/biaoyansu/10.x



## 目录
## 

## 第13章 定位方式-元素显示什么位置

### 13.1 static 
### 13.2 relative
relative 是相对自己的`"出生点"`进行定位的。 如果没有设置 top left bottomright ,和 static 位置是一样的.
```
top   向下偏移
left  向右偏移
right 向左偏移
bottom  向上偏移
```
详解 [13.4.html](front-biaoyansu/html5_css3_core/13.0/13.4.html)

### 13.3 absolute

- 绝对定位 是相对于首屏的。

- 绝对定位的参照物 是可以变化的，  如果`绝对定位的的祖先`中有`相对定位`的元素， 那么 绝对定位的元素参照物，会选择 **最近的**`相对定位`的祖先 作为参照物的。

详解 [13.6.html](front-biaoyansu/html5_css3_core/13.0/13.6.html)

### 13.4 fixed
### 13.5 sticky



## 第16章 浮动

## 书籍课程代码
[HTML5与CSS3核心技法 代码](https://book1.biaoyansu.com/)