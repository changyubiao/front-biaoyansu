// 添加多个样式，可以链式编程 
// $('.a')
// .css('color','red')
// .css('background','rgba(0, 0, 0, 0.1)');




//1. 可以传入一个对象 也可以直接修改 样式
// $('.a')
// .css({
// 	color:"red",
// 	background:'rgba(0, 0, 0, 0.1)',
// 	border:'1px solid black',

// 	// 或者使用驼峰命名法
// 	"font-weight":"bolder",

// 	// 使用引号
// 	"text-align":'center'
// })
// ;

// 2. addClass 很方便
// var $a = $('.a')
// 	.addClass('black')
// 	.removeClass('b');



// //3 hasClass 判断有没有这个类 
// console.log($a.hasClass('black'));
// console.log($a.hasClass('b'));
// console.log($a.hasClass('frank'));


// 4. hide
var $a = $('.a');

// 隐藏
// $a.hide();

// 显示
// $a.show();


// 给个时间 慢慢地隐藏 单位毫秒  淡出
// $a.fadeOut(2000);


// 淡入 效果
$b = $('div.hidden');
// $b.show();
// $b.hide();

// 淡入效果
// $b.fadeIn(2000);


// 滑出效果
// $b.slideUp(2000);



// 滑入效果
$b.hide();
$b.slideDown( 3000 );













/*
	https://learn.jquery.com/using-jquery-core/
	
	
	
	# hide 隐藏
	https://learn.jquery.com/effects/intro-to-effects/
*/
