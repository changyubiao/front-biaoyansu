//1. 从div 开始找
// $('div').find('.child').css('border','2px solid #444');

// 从 上往下 找   指定 父级
$('.grand-pa').find('.child').css('border', '2px solid #444');




//2. 从儿子 向上找父亲
// $('#child1').parent().css('border','2px solid blue')


//2.1  向上找，指定类 可以是 父亲，爷爷，祖爷爷 等等。。。
$('#child1').parents('.grand-pa').css('border', '2px solid blue');




// 3. filter 进行过滤 
$('.child').filter('.beautiful').css('color', 'white');
