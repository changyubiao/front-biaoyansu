

# jquery 快速入门 

[jquery 快速入门 课程地址](https://biaoyansu.com/16.x)



## 目录



## 16.1 jQuery 引入
## 16.2 jQuery 选择器
## 16.3 过滤器
## 16.4 操作样式

jquery 选择后 可以通过 .css() 添加样式，也可以传入一个对象，作为样式，如果样式 有 `font-size` 可以添加引号，或者 使用 `fontSize` 即可。 
	
在选择的结点 添加类 
$('xx').addClass('类名') 
	`removeClass`, `hasClass` 对应方法
	
$('xx').show()
$('xx').hide()

淡入淡出操作 
$a.fadeOut(2000);
$b.fadeIn(2000);


```js

// # 向上滑动
$b.slideUp(2000);

// 向下滑动
$b.slideDown( 3000 );

```

[intro-to-effects](	https://learn.jquery.com/effects/intro-to-effects/)

  


## 16.5 操作dom
[manipulating-elements docs](https://learn.jquery.com/using-jquery-core/manipulating-elements/)

## 16.6 事件管理
## 16.7 获取元素属性

主要的三个方法

attr  prop  removeAttr
## 16.8  表单及输入
## 16.9 Ajax-load 方法

```javascript


// 1.ajax 基本使用

const gitToken = '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';

$.ajax('https://api.github.com/users/changyubiao',{
	method:'GET',
	headers: {
		"Authorization": `token ${gitToken}`
	},
}).done(function(data){
	console.log(data);
});

```

这些方法 都可以 简写形式
`$.get(), $.getScript(), $.getJSON(), $.post(), and $().load()`










## 参考地址

[jquery 学习 runoob](https://www.runoob.com/jquery/jquery-tutorial.html)
[jquery 官网]( https://jquery.com/)
[about-jquery](https://learn.jquery.com/about-jquery/)
				 
[css-styling-dimensions](https://learn.jquery.com/using-jquery-core/css-styling-dimensions/)
[ajax api](https://api.jquery.com/jQuery.ajax/)
[ajax method](https://learn.jquery.com/ajax/jquery-ajax-methods/)