// console.log(jQuery);
// document.getElementById('a').style.background = 'skyblue';



// 1. 
// document.querySelector('#a')
// 	.style
// 	.background = 'skyblue';


// jQuery('#b').css('background', 'red');
// $('#b').css('background', 'red');


/*
document.querySelector('#a')
document.getElementById('a')

$('#b')

*/

/*

JQUERY  选择器 和 css 选择器 几乎是一样的， 直接选即可，
*/

// 2.  $('selector')  css 的选择器

// $('div').css('color','#ccc');


$('#a').css('background', '#eee');

$('#a p ').css('border', '1px solid red');
jQuery('a').css('text-decoration', 'none');


// 没有生效呢？
jQuery('a:hover').css('color', 'black');


jQuery('.nav').css('background', '#ccc');
jQuery('.nav').css('border-radius', '5px');
jQuery('.nav ul').css('display', 'block');
jQuery('.nav ul').css('line-height', '50px');
jQuery('.nav ul li').css('list-style', 'none');
jQuery('.nav ul li').css('display', 'inline-block');
jQuery('.nav ul li').css('padding-right', '30px');


jQuery('input[type="number"]').css('background', 'yellow');




// // 支持伪类 
// 选第一个元素 
jQuery('.nav ul li:first').css('font-weight','bolder');



// 选最后一个元素 
jQuery('.nav ul li:last').css('font-weight','bolder');

// add  event  

// $("#btn-1").hover(
// 	function() {
// 		alert("你进入了 btn-1!");
// 	}
// 	// function() {
// 	// 	alert("拜拜! 现在你离开了 btn-1!");
// 	// }
// );


// add event 
// $('#btn-1').click(function(){
// 	console.log('click button...');
// })



// $(document).ready(function(){
	
// 	console.log('dom load done');
// })