// console.log(jQuery);


/*
https://learn.jquery.com/using-jquery-core/attributes/



$( "a" ).attr( "href", "allMyHrefsAreTheSameNow.html" );
 
$( "a" ).attr({
    title: "all titles are the same too!",
    href: "somethingNew.html"
});


$( "a" ).attr( "href" );

// 处理显性的属性，  get set
attr()
 
 
 // 处理隐性的属性
prop()

// 移除属性
removeAttr()




// 可以拿到值 dom 结点的属性
$a.prop('baseURI')
'http://127.0.0.1:8849/front-biaoyansu/16.x/16.7/index.html'

// 拿不到 ， 显示 html标签里面的属性 是可以拿到，dom 结点的属性 很多都拿不到 
$a.attr('baseUrRL');
undefined




有种说法 Attribute 叫做特性，
Property和后端一样叫做属性，VS里面用一个扳手来表示。一定是这样的。(๑•̀ㅂ•́)و✧



*/ 


var $a = $('#a');


// set 
$a.attr('href','http://www.b.com');

$a.prop('href','http://www.b.com');



// get 
$a.attr('href');


// removeAttr  
$a.attr('aaa','aaa value');
$a.removeAttr('aaa');
$a.removeAttr('href');




// // 可以拿到值 dom 结点的属性
// $a.prop('baseURI')
// 'http://127.0.0.1:8849/front-biaoyansu/16.x/16.7/index.html'

// // 拿不到 ， 显示 html标签里面的属性 是可以拿到，dom 结点的属性 很多都拿不到 
// $a.attr('baseUrRL');
// undefined

