

# BOOTSTRAP 学习

[玩转Bootstrap - 表严肃讲Bootstrap 课程地址](https://biaoyansu.com/14.x)




## 目录 
## 14.1 栅格系统  
## 14.2 表单 
## 14.3 按钮 
## 14.4 按钮组 
## 14.5 局部导航 
## 14.6 导航栏
## 14.7 面板
## 14.8 表格
## 14.8 其他组件
alert 警告框，列表组，巨幕，标签，徽章-badge ，路径导航 等



## 参考文档 
- [按钮](https://v3.bootcss.com/css/#buttons)
- [栅格系统 文档](https://v3.bootcss.com/css/#grid)
- [表单系统](https://v3.bootcss.com/css/#forms)
- [默认分页](https://v3.bootcss.com/components/#pagination-default)
- [分页](https://v3.bootcss.com/components/#pagination)
- [breadcrumbs 路径导航](https://v3.bootcss.com/components/#breadcrumbs)
- [警告框](https://v3.bootcss.com/components/#alerts)
- [徽章](https://v3.bootcss.com/components/#badges)
- [标签](https://v3.bootcss.com/components/#labels)
- [巨幕](https://v3.bootcss.com/components/#jumbotron)
- [bootcdn](https://www.bootcdn.cn/)
- [响应式工具](https://v3.bootcss.com/css/#responsive-utilities)
