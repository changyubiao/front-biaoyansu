# front-biaoyansu

#### 介绍
前端学习资料，主要是学习 https://biaoyansu.com/   表严肃 网站的学习记录

#### 软件架构
软件架构说明


### 目录 


- html5与css3 核心技法  [html5与css3核心技法](#)
- 9.x  [CSS火速入门](https://gitee.com/changyubiao/front-biaoyansu/blob/master/9.x/README.md)
- 10.x [王花花博客实战](https://gitee.com/changyubiao/front-biaoyansu/blob/master/10.x/README.md)
- 11.x [电商站首页布局-蘑菇街](https://gitee.com/changyubiao/front-biaoyansu/blob/master/11.x/README.md)
- 13.x [王花花博客-响应式布局改造](https://gitee.com/changyubiao/front-biaoyansu/blob/master/13.x/README.md)

- 12.x [javascript精讲](https://gitee.com/changyubiao/front-biaoyansu/blob/master/12.x/README.md)

- 14.x [BOOTSTRAP 学习](https://gitee.com/changyubiao/front-biaoyansu/blob/master/14.x/README.md)

- 15.x [BOOTSTRAP实战新闻开发](https://gitee.com/changyubiao/front-biaoyansu/blob/master/15.x/README.md)
- 16.x [jquery 快速入门 ](https://gitee.com/changyubiao/front-biaoyansu/blob/master/16.x/README.md)

- 17.x [jQuery三步搞定表单验证](https://gitee.com/changyubiao/front-biaoyansu/blob/master/17.x/README.md)

- 18.x [Vue.js精讲 ](https://gitee.com/changyubiao/front-biaoyansu/blob/master/18.x/README.md)

- 22.x [做个Vue.js  TODO清单应用 ](https://gitee.com/changyubiao/front-biaoyansu/blob/master/22.x/README.md)

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
